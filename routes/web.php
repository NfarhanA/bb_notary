<?php

use Illuminate\Support\Facades\Route;

// Controllers
use App\Http\Controllers\dashboard;
use App\Http\Controllers\archive;
use App\Http\Controllers\letter;
use App\Http\Controllers\progress;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', [dashboard::class, 'show'])->name('dashboard');
Route::middleware(['auth:sanctum', 'verified'])->get('/archive', [archive::class, 'show'])->name('archive');
Route::middleware(['auth:sanctum', 'verified'])->get('/letter', [letter::class, 'display'])->name('letter');
Route::middleware(['auth:sanctum', 'verified'])->get('/progress', [progress::class, 'show'])->name('progress');
