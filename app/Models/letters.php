<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class letters extends Model
{
    use HasFactory;

    protected $fillable=[
      'id','no_document','type','created_at','updated_at'
    ];
}
