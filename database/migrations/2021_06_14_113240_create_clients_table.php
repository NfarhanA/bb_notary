<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->string('nik',17)->unique();
            $table->string('no_kk',17);
            $table->timestamp('tanggal_lahir');
            $table->string('jenis_kelamin',7);
            $table->text('alamat');
            $table->string('status_perkawinan',12);
            $table->string('pekerjaan',50);
            $table->string('f_ktp',50)->nullable();
            $table->string('f_kk',50)->nullable();
            $table->string('f_npwp',50)->nullable();
            $table->string('f_sertifikat',50)->nullable();
            $table->string('f_bukunikah',50)->nullable();
            $table->string('f_pbb',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
