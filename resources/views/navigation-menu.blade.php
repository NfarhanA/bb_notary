<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
  <style >
      /* differentiate and re arrange icons and texts when they are coliding to each other */
    .icons,
    .texts {
      vertical-align: middle;
      display: inline-block;
    }

    /* switching effect for hovered content or icon */
    .dashi:hover .icon-unlock,
    .dashi .icon-lock {
        display: none;
    }
    .dashi:hover .icon-lock {
        display: inline;
    }
  </style>
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="flex-shrink-0 flex items-center">
                    <a href="{{ route('dashboard') }}">
                        <x-jet-application-mark class="block h-9 w-auto" />
                    </a>
                </div>

                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-3 sm:flex">
                    <x-jet-nav-link class="dashi" style="text-decoration:none;" href="{{ route('dashboard') }}" :active="request()->routeIs('dashboard')">
                      <!-- Switching Icon -->
                        <div class="icons">
                          <i class="icon-unlock">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-columns-gap" viewBox="0 0 16 16">
                              <path d="M6 1v3H1V1h5zM1 0a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1H1zm14 12v3h-5v-3h5zm-5-1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1h-5zM6 8v7H1V8h5zM1 7a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1V8a1 1 0 0 0-1-1H1zm14-6v7h-5V1h5zm-5-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1h-5z"/>
                            </svg>
                          </i>
                          <i class="icon-lock" >
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-columns" viewBox="0 0 16 16">
                              <path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V2zm8.5 0v8H15V2H8.5zm0 9v3H15v-3H8.5zm-1-9H1v3h6.5V2zM1 14h6.5V6H1v8z"/>
                            </svg>
                          </i>
                        </div>
                        <div class="texts">
                          &nbsp;
                          {{ __('Dashboard') }}
                        </div>
                    </x-jet-nav-link>
                    <x-jet-nav-link class="dashi" style="text-decoration:none;" href="{{ route('archive') }}" :active="request()->routeIs('archive')">
                      <!-- Switching/Responsive Icon -->
                      <div class="icons">
                          <i class="icon-unlock">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-folder" viewBox="0 0 16 16">
                              <path d="M.54 3.87.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3h3.982a2 2 0 0 1 1.992 2.181l-.637 7A2 2 0 0 1 13.174 14H2.826a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31zM2.19 4a1 1 0 0 0-.996 1.09l.637 7a1 1 0 0 0 .995.91h10.348a1 1 0 0 0 .995-.91l.637-7A1 1 0 0 0 13.81 4H2.19zm4.69-1.707A1 1 0 0 0 6.172 2H2.5a1 1 0 0 0-1 .981l.006.139C1.72 3.042 1.95 3 2.19 3h5.396l-.707-.707z"/>
                            </svg>
                          </i>
                          <i class="icon-lock" >
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-folder2-open" viewBox="0 0 16 16">
                              <path d="M1 3.5A1.5 1.5 0 0 1 2.5 2h2.764c.958 0 1.76.56 2.311 1.184C7.985 3.648 8.48 4 9 4h4.5A1.5 1.5 0 0 1 15 5.5v.64c.57.265.94.876.856 1.546l-.64 5.124A2.5 2.5 0 0 1 12.733 15H3.266a2.5 2.5 0 0 1-2.481-2.19l-.64-5.124A1.5 1.5 0 0 1 1 6.14V3.5zM2 6h12v-.5a.5.5 0 0 0-.5-.5H9c-.964 0-1.71-.629-2.174-1.154C6.374 3.334 5.82 3 5.264 3H2.5a.5.5 0 0 0-.5.5V6zm-.367 1a.5.5 0 0 0-.496.562l.64 5.124A1.5 1.5 0 0 0 3.266 14h9.468a1.5 1.5 0 0 0 1.489-1.314l.64-5.124A.5.5 0 0 0 14.367 7H1.633z"/>
                            </svg>
                          </i>
                        </div>
                        <div class="texts">
                        &nbsp;
                        {{ __('Archive') }}
                      </div>
                    </x-jet-nav-link>
                    <x-jet-nav-link class="dashi" style="text-decoration:none;" href="{{ route('letter') }}" :active="request()->routeIs('letter')">
                      <!-- Switching/Responsive Icon -->
                      <div class="icons">
                          <i class="icon-unlock">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
                              <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
                            </svg>
                          </i>
                          <i class="icon-lock" >
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-open" viewBox="0 0 16 16">
                              <path d="M8.47 1.318a1 1 0 0 0-.94 0l-6 3.2A1 1 0 0 0 1 5.4v.818l5.724 3.465L8 8.917l1.276.766L15 6.218V5.4a1 1 0 0 0-.53-.882l-6-3.2zM15 7.388l-4.754 2.877L15 13.117v-5.73zm-.035 6.874L8 10.083l-6.965 4.18A1 1 0 0 0 2 15h12a1 1 0 0 0 .965-.738zM1 13.117l4.754-2.852L1 7.387v5.73zM7.059.435a2 2 0 0 1 1.882 0l6 3.2A2 2 0 0 1 16 5.4V14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V5.4a2 2 0 0 1 1.059-1.765l6-3.2z"/>
                            </svg>
                          </i>
                        </div>
                        <div class="texts">
                        &nbsp;
                        {{ __('Letter') }}
                      </div>
                    </x-jet-nav-link>
                    <x-jet-nav-link class="dashi" style="text-decoration:none;" href="{{ route('progress') }}" :active="request()->routeIs('progress')">
                        <!-- Switching/Responsive Icon -->
                        <div class="icons">
                          <i class="icon-unlock">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">
                              <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/>
                              <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/>
                              <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                            </svg>
                          </i>
                          <i class="icon-lock" >
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                              <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
                              <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"/>
                            </svg>
                          </i>
                        </div>
                        <div class="texts">
                        &nbsp;
                        {{ __('Works & Progress') }}
                      </div>
                    </x-jet-nav-link>
                </div>
            </div>

            <div class="hidden sm:flex sm:items-center sm:ml-6">
                <!-- Teams Dropdown -->
                @if (Laravel\Jetstream\Jetstream::hasTeamFeatures())
                    <div class="ml-3 relative">
                        <x-jet-dropdown align="right" width="60">
                            <x-slot name="trigger">
                                <span class="inline-flex rounded-md">
                                    <button type="button" class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:bg-gray-50 hover:text-gray-700 focus:outline-none focus:bg-gray-50 active:bg-gray-50 transition">
                                        {{ Auth::user()->currentTeam->name }}

                                        <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z" clip-rule="evenodd" />
                                        </svg>
                                    </button>
                                </span>
                            </x-slot>

                            <x-slot name="content">
                                <div class="w-60">
                                    <!-- Team Management -->
                                    <div class="block px-4 py-2 text-xs text-gray-400">
                                        {{ __('Manage Team') }}
                                    </div>

                                    <!-- Team Settings -->
                                    <x-jet-dropdown-link style="text-decoration:none;" href="{{ route('teams.show', Auth::user()->currentTeam->id) }}">
                                        {{ __('Team Settings') }}
                                    </x-jet-dropdown-link>

                                    @can('create', Laravel\Jetstream\Jetstream::newTeamModel())
                                        <x-jet-dropdown-link style="text-decoration:none;" href="{{ route('teams.create') }}">
                                            {{ __('Create New Team') }}
                                        </x-jet-dropdown-link>
                                    @endcan

                                    <div class="border-t border-gray-100"></div>

                                    <!-- Team Switcher -->
                                    <div class="block px-4 py-2 text-xs text-gray-400">
                                        {{ __('Switch Teams') }}
                                    </div>

                                    @foreach (Auth::user()->allTeams() as $team)
                                        <x-jet-switchable-team :team="$team" />
                                    @endforeach
                                </div>
                            </x-slot>
                        </x-jet-dropdown>
                    </div>
                @endif

                <!-- Settings Dropdown -->
                <div class="ml-3 relative">
                    <x-jet-dropdown align="right" width="48">
                        <x-slot name="trigger">
                            @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                                <button class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition">
                                    <img class="h-8 w-8 rounded-full object-cover" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                                </button>
                            @else
                                <span class="inline-flex rounded-md">
                                    <button type="button" class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none transition">
                                        {{ Auth::user()->name }}

                                        <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                        </svg>
                                    </button>
                                </span>
                            @endif
                        </x-slot>

                        <x-slot name="content">
                            <!-- Account Management -->
                            <div class="block px-4 py-2 text-xs text-gray-400">
                                {{ __('Manage Account') }}
                            </div>

                            <x-jet-dropdown-link style="text-decoration:none;" href="{{ route('profile.show') }}">
                              <!-- Icon -->
                              <span class="icons">
                                <!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                	 width="20px" height="20px" viewBox="0 0 1417.32 1417.32" style="enable-background:new 0 0 1417.32 1417.32;" xml:space="preserve">
                                <style type="text/css">
                                	.st0n{fill:#36A9E1;}
                                </style>
                                <circle class="st0n" cx="701.2" cy="300.98" r="170.08"/>
                                <path class="st0n" d="M45.26,572.07"/>
                                <path class="st0n" d="M861.82,690.27"/>
                                <path class="st0n" d="M793.83,480.56"/>
                                <path class="st0n" d="M624.29,544.38"/>
                                <path class="st0n" d="M1019.54,572.07"/>
                                <path class="st0n" d="M623.95,544.38"/>
                                <path class="st0n" d="M708.84,544.38C384.92,543.89,372.4,779.54,372.4,779.54v506.89h336.44V544.38z"/>
                                <path class="st0n" d="M708.49,544.38c323.91-0.49,336.44,235.16,336.44,235.16v506.89H708.49V544.38z"/>
                                </svg>
                              </span>
                              <!-- Text -->
                              <span class="texts">
                                {{ ('Profile') }}
                              </span>
                              </x-jet-dropdown-link>

                            @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                                <x-jet-dropdown-link href="{{ route('api-tokens.index') }}">
                                    {{ __('API Tokens') }}
                                </x-jet-dropdown-link>
                            @endif

                            <div class="border-t border-gray-100"></div>

                            <!-- Authentication -->
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <x-jet-dropdown-link style="text-decoration:none;" href="{{ route('logout') }}"
                                         onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                                <!-- icon -->
                                                <span class="icons">
                                                  <!--
                                                <svg  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="20px" height="20px" viewBox="0 0 1417.32 1417.32" style="enable-background:new 0 0 1417.32 1417.32;" xml:space="preserve">
                                                  <style type="text/css">
                                                    .st0n{fill:#36A9E1;}
                                                  </style>
                                                  <rect x="70.87" y="28.35" class="st0n" width="141.73" height="1360.63"/>
                                                  <rect x="283.46" y="644.88" class="st0n" width="680.31" height="127.56"/>
                                                  <polygon class="st0n" points="1167.36,708.66 806.57,347.87 1007.01,347.87 1367.8,708.66 "/>
                                                  <polygon class="st0n" points="1167.36,708.66 806.57,1069.45 1007.01,1069.45 1367.8,708.66 "/>
                                                  </svg> -->
                                                  <svg id="eWqTdelnPIv1"  width="40px" height="40px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 640 480" shape-rendering="geometricPrecision" text-rendering="geometricPrecision"><rect id="eWqTdelnPIv2" width="38.534872" height="353.236327" rx="0" ry="0" transform="matrix(1 0 0 1 118.09332699999999 81.08379300000001)" fill="rgb(54,169,225)" stroke="none" stroke-width="0"/><g id="eWqTdelnPIv3"><rect id="eWqTdelnPIv4" width="178.223783" height="24.084294" rx="0" ry="0" transform="matrix(1 0 0 1 187.93778249666667 245.65980925000005)" fill="rgb(54,169,225)" stroke="none" stroke-width="0"/><g id="eWqTdelnPIv5"><path id="eWqTdelnPIv6" d="M411.118916,245.659809L333.246362,97.139990L373.386854,97.139990L459.287506,245.659809L411.118916,245.659809Z" transform="matrix(1 0 0 1 -10.16307099999995 12.04214725000000)" fill="rgb(54,169,225)" stroke="none" stroke-width="1.280000"/><path id="eWqTdelnPIv7" d="M411.118916,245.659809L333.246362,97.139990L373.386854,97.139990L459.287506,245.659809L411.118916,245.659809Z" transform="matrix(1 0 0 -1 -10.22528850333333 503.36176574999990)" fill="rgb(54,169,225)" stroke="none" stroke-width="1.280000"/></g></g><script><![CDATA[!function(t,n){"object"==typeof exports&&"undefined"!=typeof module?module.exports=n():"function"==typeof define&&define.amd?define(n):(t="undefined"!=typeof globalThis?globalThis:t||self).__SVGATOR_PLAYER__=n()}(this,(function(){"use strict";function t(n){return(t="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t})(n)}function n(t,n){if(!(t instanceof n))throw new TypeError("Cannot call a class as a function")}function r(t,n){for(var r=0;r<n.length;r++){var e=n[r];e.enumerable=e.enumerable||!1,e.configurable=!0,"value"in e&&(e.writable=!0),Object.defineProperty(t,e.key,e)}}function e(t,n,e){return n&&r(t.prototype,n),e&&r(t,e),t}function i(t){return(i=Object.setPrototypeOf?Object.getPrototypeOf:function(t){return t.__proto__||Object.getPrototypeOf(t)})(t)}function o(t,n){return(o=Object.setPrototypeOf||function(t,n){return t.__proto__=n,t})(t,n)}function u(t,n){return!n||"object"!=typeof n&&"function"!=typeof n?function(t){if(void 0===t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return t}(t):n}function a(t){var n=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Boolean.prototype.valueOf.call(Reflect.construct(Boolean,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,e=i(t);if(n){var o=i(this).constructor;r=Reflect.construct(e,arguments,o)}else r=e.apply(this,arguments);return u(this,r)}}function l(t,n,r){return(l="undefined"!=typeof Reflect&&Reflect.get?Reflect.get:function(t,n,r){var e=function(t,n){for(;!Object.prototype.hasOwnProperty.call(t,n)&&null!==(t=i(t)););return t}(t,n);if(e){var o=Object.getOwnPropertyDescriptor(e,n);return o.get?o.get.call(r):o.value}})(t,n,r||t)}var f=Math.abs;function s(t){return t}function c(t,n,r){var e=1-r;return 3*r*e*(t*e+n*r)+r*r*r}function h(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,r=arguments.length>2&&void 0!==arguments[2]?arguments[2]:1,e=arguments.length>3&&void 0!==arguments[3]?arguments[3]:1;return t<0||t>1||r<0||r>1?null:f(t-n)<=1e-5&&f(r-e)<=1e-5?s:function(i){if(i<=0)return t>0?i*n/t:0===n&&r>0?i*e/r:0;if(i>=1)return r<1?1+(i-1)*(e-1)/(r-1):1===r&&t<1?1+(i-1)*(n-1)/(t-1):1;for(var o,u=0,a=1;u<a;){var l=c(t,r,o=(u+a)/2);if(f(i-l)<1e-5)break;l<i?u=o:a=o}return c(n,e,o)}}function v(){return 1}function y(t){return 1===t?1:0}function d(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:1,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0;if(1===t){if(0===n)return y;if(1===n)return v}var r=1/t;return function(t){return t>=1?1:(t+=n*r)-t%r}}function g(t){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:2;if(Number.isInteger(t))return t;var r=Math.pow(10,n);return Math.round(t*r)/r}var p=Math.PI/180;function m(t,n,r){return t>=.5?r:n}function b(t,n,r){return 0===t||n===r?n:t*(r-n)+n}function w(t,n,r){var e=b(t,n,r);return e<=0?0:e}function x(t,n,r){return 0===t?n:1===t?r:{x:b(t,n.x,r.x),y:b(t,n.y,r.y)}}function k(t,n,r){var e=function(t,n,r){return Math.round(b(t,n,r))}(t,n,r);return e<=0?0:e>=255?255:e}function A(t,n,r){return 0===t?n:1===t?r:{r:k(t,n.r,r.r),g:k(t,n.g,r.g),b:k(t,n.b,r.b),a:b(t,null==n.a?1:n.a,null==r.a?1:r.a)}}function _(t,n,r){if(0===t)return n;if(1===t)return r;var e=n.length;if(e!==r.length)return m(t,n,r);for(var i=[],o=0;o<e;o++)i.push(A(t,n[o],r[o]));return i}function S(t,n){for(var r=[],e=0;e<t;e++)r.push(n);return r}function O(t,n){if(--n<=0)return t;var r=(t=Object.assign([],t)).length;do{for(var e=0;e<r;e++)t.push(t[e])}while(--n>0);return t}var M,j=function(){function t(r){n(this,t),this.list=r,this.length=r.length}return e(t,[{key:"setAttribute",value:function(t,n){for(var r=this.list,e=0;e<this.length;e++)r[e].setAttribute(t,n)}},{key:"removeAttribute",value:function(t){for(var n=this.list,r=0;r<this.length;r++)n[r].removeAttribute(t)}},{key:"style",value:function(t,n){for(var r=this.list,e=0;e<this.length;e++)r[e].style[t]=n}}]),t}(),F=/-./g,P=function(t,n){return n.toUpperCase()};function B(t){return"function"==typeof t?t:m}function I(t){return t?"function"==typeof t?t:Array.isArray(t)?function(t){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:s;if(!Array.isArray(t))return n;switch(t.length){case 1:return d(t[0])||n;case 2:return d(t[0],t[1])||n;case 4:return h(t[0],t[1],t[2],t[3])||n}return n}(t,null):function(t,n){var r=arguments.length>2&&void 0!==arguments[2]?arguments[2]:s;switch(t){case"linear":return s;case"steps":return d(n.steps||1,n.jump||0)||r;case"bezier":case"cubic-bezier":return h(n.x1||0,n.y1||0,n.x2||0,n.y2||0)||r}return r}(t.type,t.value,null):null}function R(t,n,r){var e=arguments.length>3&&void 0!==arguments[3]&&arguments[3],i=n.length-1;if(t<=n[0].t)return e?[0,0,n[0].v]:n[0].v;if(t>=n[i].t)return e?[i,1,n[i].v]:n[i].v;var o,u=n[0],a=null;for(o=1;o<=i;o++){if(!(t>n[o].t)){a=n[o];break}u=n[o]}return null==a?e?[i,1,n[i].v]:n[i].v:u.t===a.t?e?[o,1,a.v]:a.v:(t=(t-u.t)/(a.t-u.t),u.e&&(t=u.e(t)),e?[o,t,r(t,u.v,a.v)]:r(t,u.v,a.v))}function q(t,n){var r=arguments.length>2&&void 0!==arguments[2]?arguments[2]:null;return t&&t.length?"function"!=typeof n?null:("function"!=typeof r&&(r=null),function(e){var i=R(e,t,n);return null!=i&&r&&(i=r(i)),i}):null}function E(t,n){return t.t-n.t}function T(n,r,e,i,o){var u,a="@"===e[0],l="#"===e[0],f=M[e],s=m;switch(a?(u=e.substr(1),e=u.replace(F,P)):l&&(e=e.substr(1)),t(f)){case"function":if(s=f(i,o,R,I,e,a,r,n),l)return s;break;case"string":s=q(i,B(f));break;case"object":if((s=q(i,B(f.i),f.f))&&"function"==typeof f.u)return f.u(r,s,e,a,n)}return s?function(t,n,r){var e=arguments.length>3&&void 0!==arguments[3]&&arguments[3];if(e)return t instanceof j?function(e){return t.style(n,r(e))}:function(e){return t.style[n]=r(e)};if(Array.isArray(n)){var i=n.length;return function(e){var o=r(e);if(null==o)for(var u=0;u<i;u++)t[u].removeAttribute(n);else for(var a=0;a<i;a++)t[a].setAttribute(n,o)}}return function(e){var i=r(e);null==i?t.removeAttribute(n):t.setAttribute(n,i)}}(r,e,s,a):null}function N(n,r,e,i){if(!i||"object"!==t(i))return null;var o=null,u=null;return Array.isArray(i)?u=function(t){if(!t||!t.length)return null;for(var n=0;n<t.length;n++)t[n].e&&(t[n].e=I(t[n].e));return t.sort(E)}(i):(u=i.keys,o=i.data||null),u?T(n,r,e,u,o):null}function C(t,n,r){if(!r)return null;var e=[];for(var i in r)if(r.hasOwnProperty(i)){var o=N(t,n,i,r[i]);o&&e.push(o)}return e.length?e:null}function z(t,n){if(!n.duration||n.duration<0)return null;var r=function(t,n){if(!n)return null;var r=[];if(Array.isArray(n))for(var e=n.length,i=0;i<e;i++){var o=n[i];if(2===o.length){var u=null;if("string"==typeof o[0])u=t.getElementById(o[0]);else if(Array.isArray(o[0])){u=[];for(var a=0;a<o[0].length;a++)if("string"==typeof o[0][a]){var l=t.getElementById(o[0][a]);l&&u.push(l)}u=u.length?1===u.length?u[0]:new j(u):null}if(u){var f=C(t,u,o[1]);f&&(r=r.concat(f))}}}else for(var s in n)if(n.hasOwnProperty(s)){var c=t.getElementById(s);if(c){var h=C(t,c,n[s]);h&&(r=r.concat(h))}}return r.length?r:null}(t,n.elements);return r?function(t,n){var r=arguments.length>2&&void 0!==arguments[2]?arguments[2]:1/0,e=arguments.length>3&&void 0!==arguments[3]?arguments[3]:1,i=arguments.length>4&&void 0!==arguments[4]&&arguments[4],o=arguments.length>5&&void 0!==arguments[5]?arguments[5]:1,u=t.length,a=e>0?n:0;i&&r%2==0&&(a=n-a);var l=null;return function(f,s){var c=f%n,h=1+(f-c)/n;s*=e,i&&h%2==0&&(s=-s);var v=!1;if(h>r)c=a,v=!0,-1===o&&(c=e>0?0:n);else if(s<0&&(c=n-c),c===l)return!1;l=c;for(var y=0;y<u;y++)t[y](c);return v}}(r,n.duration,n.iterations||1/0,n.direction||1,!!n.alternate,n.fill||1):null}var L=function(){function t(r,e){var i=arguments.length>2&&void 0!==arguments[2]?arguments[2]:{};n(this,t),this._id=0,this._running=!1,this._rollingBack=!1,this._animations=r,this.duration=e.duration,this.alternate=e.alternate,this.fill=e.fill,this.iterations=e.iterations,this.direction=i.direction||1,this.speed=i.speed||1,this.fps=i.fps||100,this.offset=i.offset||0,this.rollbackStartOffset=0}return e(t,[{key:"_rollback",value:function(){var t=this,n=1/0,r=null;this.rollbackStartOffset=this.offset,this._rollingBack||(this._rollingBack=!0,this._running=!0);this._id=window.requestAnimationFrame((function e(i){if(t._rollingBack){null==r&&(r=i);var o=i-r,u=t.rollbackStartOffset-o,a=Math.round(u*t.speed);if(a>t.duration&&n!=1/0){var l=!!t.alternate&&a/t.duration%2>1,f=a%t.duration;a=(f+=l?t.duration:0)||t.duration}var s=t.fps?1e3/t.fps:0,c=Math.max(0,a);if(c<n-s){t.offset=c,n=c;for(var h=t._animations,v=h.length,y=0;y<v;y++)h[y](c,t.direction)}var d=!1;if(t.iterations>0&&-1===t.fill){var g=t.iterations*t.duration,p=g==a;a=p?0:a,t.offset=p?0:t.offset,d=a>g}a>0&&t.offset>=a&&!d?t._id=window.requestAnimationFrame(e):t.stop()}}))}},{key:"_start",value:function(){var t=this,n=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,r=-1/0,e=null,i={},o=function o(u){t._running=!0,null==e&&(e=u);var a=Math.round((u-e+n)*t.speed),l=t.fps?1e3/t.fps:0;if(a>r+l&&!t._rollingBack){t.offset=a,r=a;for(var f=t._animations,s=f.length,c=0,h=0;h<s;h++)i[h]?c++:(i[h]=f[h](a,t.direction),i[h]&&c++);if(c===s)return void t._stop()}t._id=window.requestAnimationFrame(o)};this._id=window.requestAnimationFrame(o)}},{key:"_stop",value:function(){this._id&&window.cancelAnimationFrame(this._id),this._running=!1,this._rollingBack=!1}},{key:"play",value:function(){!this._rollingBack&&this._running||(this._rollingBack=!1,this.rollbackStartOffset>this.duration&&(this.offset=this.rollbackStartOffset-(this.rollbackStartOffset-this.offset)%this.duration,this.rollbackStartOffset=0),this._start(this.offset))}},{key:"stop",value:function(){this._stop(),this.offset=0,this.rollbackStartOffset=0;var t=this.direction,n=this._animations;window.requestAnimationFrame((function(){for(var r=0;r<n.length;r++)n[r](0,t)}))}},{key:"reachedToEnd",value:function(){return this.iterations>0&&this.offset>=this.iterations*this.duration}},{key:"restart",value:function(){this._stop(),this.offset=0,this._start()}},{key:"pause",value:function(){this._stop()}},{key:"reverse",value:function(){this.direction=-this.direction}}],[{key:"build",value:function(n,r){return(n=function(t,n){if(M=n,!t||!t.root||!Array.isArray(t.animations))return null;for(var r=document.getElementsByTagName("svg"),e=!1,i=0;i<r.length;i++)if(r[i].id===t.root&&!r[i].svgatorAnimation){(e=r[i]).svgatorAnimation=!0;break}if(!e)return null;var o=t.animations.map((function(t){return z(e,t)})).filter((function(t){return!!t}));return o.length?{element:e,animations:o,animationSettings:t.animationSettings,options:t.options||void 0}:null}(n,r))?{el:n.element,options:n.options||{},player:new t(n.animations,n.animationSettings,n.options)}:null}}]),t}();!function(){for(var t=0,n=["ms","moz","webkit","o"],r=0;r<n.length&&!window.requestAnimationFrame;++r)window.requestAnimationFrame=window[n[r]+"RequestAnimationFrame"],window.cancelAnimationFrame=window[n[r]+"CancelAnimationFrame"]||window[n[r]+"CancelRequestAnimationFrame"];window.requestAnimationFrame||(window.requestAnimationFrame=function(n){var r=Date.now(),e=Math.max(0,16-(r-t)),i=window.setTimeout((function(){n(r+e)}),e);return t=r+e,i},window.cancelAnimationFrame=window.clearTimeout)}();var D=/\.0+$/g;function Q(t){return Number.isInteger(t)?t+"":t.toFixed(6).replace(D,"")}function U(t){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:" ";return t&&t.length?t.map(Q).join(n):""}function V(t){return t?null==t.a||t.a>=1?"rgb("+t.r+","+t.g+","+t.b+")":"rgba("+t.r+","+t.g+","+t.b+","+t.a+")":"transparent"}var $={f:null,i:function(t,n,r){return 0===t?n:1===t?r:{x:w(t,n.x,r.x),y:w(t,n.y,r.y)}},u:function(t,n){return function(r){var e=n(r);t.setAttribute("rx",Q(e.x)),t.setAttribute("ry",Q(e.y))}}},G={f:null,i:function(t,n,r){return 0===t?n:1===t?r:{width:w(t,n.width,r.width),height:w(t,n.height,r.height)}},u:function(t,n){return function(r){var e=n(r);t.setAttribute("width",Q(e.width)),t.setAttribute("height",Q(e.height))}}},H=Math.sin,Y=Math.cos,Z=Math.acos,J=Math.asin,K=Math.tan,W=Math.atan2,X=Math.PI/180,tt=180/Math.PI,nt=Math.sqrt,rt=function(){function t(){var r=arguments.length>0&&void 0!==arguments[0]?arguments[0]:1,e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,i=arguments.length>2&&void 0!==arguments[2]?arguments[2]:0,o=arguments.length>3&&void 0!==arguments[3]?arguments[3]:1,u=arguments.length>4&&void 0!==arguments[4]?arguments[4]:0,a=arguments.length>5&&void 0!==arguments[5]?arguments[5]:0;n(this,t),this.m=[r,e,i,o,u,a],this.i=null,this.w=null,this.s=null}return e(t,[{key:"determinant",get:function(){var t=this.m;return t[0]*t[3]-t[1]*t[2]}},{key:"isIdentity",get:function(){if(null===this.i){var t=this.m;this.i=1===t[0]&&0===t[1]&&0===t[2]&&1===t[3]&&0===t[4]&&0===t[5]}return this.i}},{key:"point",value:function(t,n){var r=this.m;return{x:r[0]*t+r[2]*n+r[4],y:r[1]*t+r[3]*n+r[5]}}},{key:"translateSelf",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0;if(!t&&!n)return this;var r=this.m;return r[4]+=r[0]*t+r[2]*n,r[5]+=r[1]*t+r[3]*n,this.w=this.s=this.i=null,this}},{key:"rotateSelf",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0;if(t%=360){var n=H(t*=X),r=Y(t),e=this.m,i=e[0],o=e[1];e[0]=i*r+e[2]*n,e[1]=o*r+e[3]*n,e[2]=e[2]*r-i*n,e[3]=e[3]*r-o*n,this.w=this.s=this.i=null}return this}},{key:"scaleSelf",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:1,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:1;if(1!==t||1!==n){var r=this.m;r[0]*=t,r[1]*=t,r[2]*=n,r[3]*=n,this.w=this.s=this.i=null}return this}},{key:"skewSelf",value:function(t,n){if(n%=360,(t%=360)||n){var r=this.m,e=r[0],i=r[1],o=r[2],u=r[3];t&&(t=K(t*X),r[2]+=e*t,r[3]+=i*t),n&&(n=K(n*X),r[0]+=o*n,r[1]+=u*n),this.w=this.s=this.i=null}return this}},{key:"resetSelf",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:1,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,r=arguments.length>2&&void 0!==arguments[2]?arguments[2]:0,e=arguments.length>3&&void 0!==arguments[3]?arguments[3]:1,i=arguments.length>4&&void 0!==arguments[4]?arguments[4]:0,o=arguments.length>5&&void 0!==arguments[5]?arguments[5]:0,u=this.m;return u[0]=t,u[1]=n,u[2]=r,u[3]=e,u[4]=i,u[5]=o,this.w=this.s=this.i=null,this}},{key:"recomposeSelf",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:null,r=arguments.length>2&&void 0!==arguments[2]?arguments[2]:null,e=arguments.length>3&&void 0!==arguments[3]?arguments[3]:null,i=arguments.length>4&&void 0!==arguments[4]?arguments[4]:null;return this.isIdentity||this.resetSelf(),t&&(t.x||t.y)&&this.translateSelf(t.x,t.y),n&&this.rotateSelf(n),r&&(r.x&&this.skewSelf(r.x,0),r.y&&this.skewSelf(0,r.y)),!e||1===e.x&&1===e.y||this.scaleSelf(e.x,e.y),i&&(i.x||i.y)&&this.translateSelf(i.x,i.y),this}},{key:"decompose",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,r=this.m,e=r[0]*r[0]+r[1]*r[1],i=[[r[0],r[1]],[r[2],r[3]]],o=nt(e);if(0===o)return{origin:{x:r[4],y:r[5]},translate:{x:t,y:n},scale:{x:0,y:0},skew:{x:0,y:0},rotate:0};i[0][0]/=o,i[0][1]/=o;var u=r[0]*r[3]-r[1]*r[2]<0;u&&(o=-o);var a=i[0][0]*i[1][0]+i[0][1]*i[1][1];i[1][0]-=i[0][0]*a,i[1][1]-=i[0][1]*a;var l=nt(i[1][0]*i[1][0]+i[1][1]*i[1][1]);if(0===l)return{origin:{x:r[4],y:r[5]},translate:{x:t,y:n},scale:{x:o,y:0},skew:{x:0,y:0},rotate:0};i[1][0]/=l,i[1][1]/=l,a/=l;var f=0;return i[1][1]<0?(f=Z(i[1][1])*tt,i[0][1]<0&&(f=360-f)):f=J(i[0][1])*tt,u&&(f=-f),a=W(a,nt(i[0][0]*i[0][0]+i[0][1]*i[0][1]))*tt,u&&(a=-a),{origin:{x:r[4],y:r[5]},translate:{x:t,y:n},scale:{x:o,y:l},skew:{x:a,y:0},rotate:f}}},{key:"toString",value:function(){return null===this.s&&(this.s="matrix("+this.m.map(it).join(" ")+")"),this.s}}]),t}(),et=/\.0+$/;function it(t){return Number.isInteger(t)?t:t.toFixed(14).replace(et,"")}function ot(t,n,r){return t+(n-t)*r}function ut(t,n,r){var e=arguments.length>3&&void 0!==arguments[3]&&arguments[3],i={x:ot(t.x,n.x,r),y:ot(t.y,n.y,r)};return e&&(i.a=at(t,n)),i}function at(t,n){return Math.atan2(n.y-t.y,n.x-t.x)}Object.freeze({M:2,L:2,Z:0,H:1,V:1,C:6,Q:4,T:2,S:4,A:7});var lt={},ft=null;function st(n){var r=function(){if(ft)return ft;if("object"!==("undefined"==typeof document?"undefined":t(document)))return{};var n=document.createElementNS("http://www.w3.org/2000/svg","svg");return n.style.position="absolute",n.style.opacity="0.01",n.style.zIndex="-9999",n.style.left="-9999px",n.style.width="1px",n.style.height="1px",ft={svg:n}}().svg;if(!r)return function(t){return null};var e=document.createElementNS(r.namespaceURI,"path");e.setAttributeNS(null,"d",n),e.setAttributeNS(null,"fill","none"),e.setAttributeNS(null,"stroke","none"),r.appendChild(e);var i=e.getTotalLength();return function(t){var n=e.getPointAtLength(i*t);return{x:n.x,y:n.y}}}function ct(t){return lt[t]?lt[t]:lt[t]=st(t)}function ht(t,n,r,e){if(!t||!e)return!1;var i=["M",t.x,t.y];if(n&&r&&(i.push("C"),i.push(n.x),i.push(n.y),i.push(r.x),i.push(r.y)),n?!r:r){var o=n||r;i.push("Q"),i.push(o.x),i.push(o.y)}return n||r||i.push("L"),i.push(e.x),i.push(e.y),i.join(" ")}function vt(t,n,r,e){var i=arguments.length>4&&void 0!==arguments[4]?arguments[4]:1,o=ht(t,n,r,e),u=ct(o);try{return u(i)}catch(t){return null}}function yt(t,n,r,e){var i=1-e;return i*i*t+2*i*e*n+e*e*r}function dt(t,n,r,e){return 2*(1-e)*(n-t)+2*e*(r-n)}function gt(t,n,r,e){var i=arguments.length>4&&void 0!==arguments[4]&&arguments[4],o=vt(t,n,null,r,e);return o||(o={x:yt(t.x,n.x,r.x,e),y:yt(t.y,n.y,r.y,e)}),i&&(o.a=pt(t,n,r,e)),o}function pt(t,n,r,e){return Math.atan2(dt(t.y,n.y,r.y,e),dt(t.x,n.x,r.x,e))}function mt(t,n,r,e,i){var o=i*i;return i*o*(e-t+3*(n-r))+3*o*(t+r-2*n)+3*i*(n-t)+t}function bt(t,n,r,e,i){var o=1-i;return 3*(o*o*(n-t)+2*o*i*(r-n)+i*i*(e-r))}function wt(t,n,r,e,i){var o=arguments.length>5&&void 0!==arguments[5]&&arguments[5],u=vt(t,n,r,e,i);return u||(u={x:mt(t.x,n.x,r.x,e.x,i),y:mt(t.y,n.y,r.y,e.y,i)}),o&&(u.a=xt(t,n,r,e,i)),u}function xt(t,n,r,e,i){return Math.atan2(bt(t.y,n.y,r.y,e.y,i),bt(t.x,n.x,r.x,e.x,i))}function kt(t,n,r){var e=arguments.length>3&&void 0!==arguments[3]&&arguments[3];if(_t(n)){if(St(r))return gt(n,r.start,r,t,e)}else if(_t(r)){if(n.end)return gt(n,n.end,r,t,e)}else{if(n.end)return r.start?wt(n,n.end,r.start,r,t,e):gt(n,n.end,r,t,e);if(r.start)return gt(n,r.start,r,t,e)}return ut(n,r,t,e)}function At(t,n,r){var e=kt(t,n,r,!0);return e.a=function(t){var n=arguments.length>1&&void 0!==arguments[1]&&arguments[1];return n?t+Math.PI:t}(e.a)/p,e}function _t(t){return!t.type||"corner"===t.type}function St(t){return null!=t.start&&!_t(t)}var Ot=new rt;var Mt={f:Q,i:b},jt={f:Q,i:function(t,n,r){var e=b(t,n,r);return e<=0?0:e>=1?1:e}};function Ft(t,n,r,e,i,o,u,a){return n=function(t,n,r){for(var e,i,o,u=t.length-1,a={},l=0;l<=u;l++)(e=t[l]).e&&(e.e=n(e.e)),e.v&&"g"===(i=e.v).t&&i.r&&(o=r.getElementById(i.r))&&(a[i.r]=o.querySelectorAll("stop"));return a}(t,e,a),function(e){var i,o=r(e,t,Pt);return o?"c"===o.t?V(o.v):"g"===o.t?(n[o.r]&&function(t,n){for(var r=0,e=t.length;r<e;r++)t[r].setAttribute("stop-color",V(n[r]))}(n[o.r],o.v),(i=o.r)?"url(#"+i+")":"none"):"none":"none"}}function Pt(t,n,r){if(0===t)return n;if(1===t)return r;if(n&&r){var e=n.t;if(e===r.t)switch(n.t){case"c":return{t:e,v:A(t,n.v,r.v)};case"g":if(n.r===r.r)return{t:e,v:_(t,n.v,r.v),r:n.r}}}return m(t,n,r)}var Bt={fill:Ft,"fill-opacity":jt,stroke:Ft,"stroke-opacity":jt,"stroke-width":Mt,"stroke-dashoffset":{f:Q,i:b},"stroke-dasharray":{f:function(t){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:" ";return t&&t.length>0&&(t=t.map((function(t){return Math.floor(1e4*t)/1e4}))),U(t,n)},i:function(t,n,r){var e,i,o,u=n.length,a=r.length;if(u!==a)if(0===u)n=S(u=a,0);else if(0===a)a=u,r=S(u,0);else{var l=(o=(e=u)*(i=a)/function(t,n){for(var r;n;)r=n,n=t%n,t=r;return t||1}(e,i))<0?-o:o;n=O(n,Math.floor(l/u)),r=O(r,Math.floor(l/a)),u=a=l}for(var f=[],s=0;s<u;s++)f.push(g(w(t,n[s],r[s]),6));return f}},opacity:jt,transform:function(n,r,e,i){if(!(n=function(n,r){if(!n||"object"!==t(n))return null;var e=!1;for(var i in n)n.hasOwnProperty(i)&&(n[i]&&n[i].length?(n[i].forEach((function(t){t.e&&(t.e=r(t.e))})),e=!0):delete n[i]);return e?n:null}(n,i)))return null;var o=function(t,i,o){var u=arguments.length>3&&void 0!==arguments[3]?arguments[3]:null;return n[t]?e(i,n[t],o):r&&r[t]?r[t]:u};return r&&r.a&&n.o?function(t){var r=e(t,n.o,At);return Ot.recomposeSelf(r,o("r",t,b,0)+r.a,o("k",t,x),o("s",t,x),o("t",t,x)).toString()}:function(t){return Ot.recomposeSelf(o("o",t,kt,null),o("r",t,b,0),o("k",t,x),o("s",t,x),o("t",t,x)).toString()}},r:Mt,"#size":G,"#radius":$,_:function(t,n){if(Array.isArray(t))for(var r=0;r<t.length;r++)this[t[r]]=n;else this[t]=n}};return function(t){!function(t,n){if("function"!=typeof n&&null!==n)throw new TypeError("Super expression must either be null or a function");t.prototype=Object.create(n&&n.prototype,{constructor:{value:t,writable:!0,configurable:!0}}),n&&o(t,n)}(u,t);var r=a(u);function u(){return n(this,u),r.apply(this,arguments)}return e(u,null,[{key:"build",value:function(t){var n=l(i(u),"build",this).call(this,t,Bt);n.el,n.options;var r=n.player;return function(t,n,r){t.play()}(r),r}}]),u}(L)}));
                                                  __SVGATOR_PLAYER__.build({"root":"eWqTdelnPIv1","animations":[{"duration":3000,"direction":1,"iterations":0,"fill":1,"alternate":false,"speed":1,"elements":{"eWqTdelnPIv4":{"transform":{"data":{"t":{"x":-89.11189150000001,"y":-12.04214725}},"keys":{"o":[{"t":0,"v":{"x":277.0496739966667,"y":257.70195650000005,"type":"corner"}},{"t":500,"v":{"x":460.89312599666664,"y":257.70195650000005,"type":"corner"}},{"t":1000,"v":{"x":278.51856524666664,"y":257.70195650000005,"type":"corner"}},{"t":1500,"v":{"x":460.89312599666664,"y":257.70195650000005,"type":"corner"}},{"t":2000,"v":{"x":277.0496739966667,"y":257.70195650000005,"type":"corner"}},{"t":2500,"v":{"x":460.89312599666664,"y":257.70195650000005,"type":"corner"}},{"t":3000,"v":{"x":278.5869291216666,"y":257.70195650000005,"type":"corner"}}]}}},"eWqTdelnPIv6":{"transform":{"data":{"t":{"x":-396.266934,"y":-171.399899625}},"keys":{"o":[{"t":0,"v":{"x":386.10386300000005,"y":183.442046875,"type":"corner"}},{"t":500,"v":{"x":569.947315,"y":183.44204687500002,"type":"corner"}},{"t":1000,"v":{"x":387.57275425,"y":183.44204687500002,"type":"corner"}},{"t":1500,"v":{"x":569.947315,"y":183.44204687500002,"type":"corner"}},{"t":2000,"v":{"x":387.57275425,"y":183.44204687500002,"type":"corner"}},{"t":2500,"v":{"x":569.81058725,"y":183.44204687500002,"type":"corner"}},{"t":3000,"v":{"x":387.504390375,"y":183.44204687500002,"type":"corner"}}]}}},"eWqTdelnPIv7":{"transform":{"data":{"s":{"x":1,"y":-1},"t":{"x":-396.266934,"y":-171.399899625}},"keys":{"o":[{"t":0,"v":{"x":386.04164549666666,"y":331.9618661249999,"type":"corner"}},{"t":500,"v":{"x":569.8850974966666,"y":331.9618661249999,"type":"corner"}},{"t":1000,"v":{"x":387.5105367466666,"y":331.9618661249999,"type":"corner"}},{"t":1500,"v":{"x":569.9473149966666,"y":331.9618661249999,"type":"corner"}},{"t":2000,"v":{"x":387.5105367466666,"y":331.9618661249999,"type":"corner"}},{"t":2500,"v":{"x":569.7483697466666,"y":331.9618661249999,"type":"corner"}},{"t":3000,"v":{"x":387.4421728716666,"y":331.9618661249999,"type":"corner"}}]}}}}}],"options":{"start":"load","hover":"freeze","click":"freeze","scroll":25,"font":"embed","exportedIds":"unique","svgFormat":"animated","title":"Untitled"},"animationSettings":{"duration":3000,"direction":1,"iterations":0,"fill":1,"alternate":false,"speed":1}})]]></script>
                                                </svg>
                                                  </span>
                                                <!-- Text -->
                                                <span class="texts">
                                                  {{ ('Log Out') }}
                                                </span>
                                </x-jet-dropdown-link>
                            </form>
                        </x-slot>
                    </x-jet-dropdown>
                </div>
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <x-jet-responsive-nav-link style="text-decoration:none;"  href="{{ route('dashboard') }}" :active="request()->routeIs('dashboard')">
                {{ __('Dashboard') }}
            </x-jet-responsive-nav-link>
            <x-jet-responsive-nav-link style="text-decoration:none;"  href="{{ route('archive') }}" :active="request()->routeIs('archive')">
                {{ __('Archive') }}
            </x-jet-responsive-nav-link>
            <x-jet-responsive-nav-link style="text-decoration:none;"  href="{{ route('letter') }}" :active="request()->routeIs('letter')">
                {{ __('Letter') }}
            </x-jet-responsive-nav-link>
            <x-jet-responsive-nav-link style="text-decoration:none;"  href="{{ route('progress') }}" :active="request()->routeIs('progress')">
                {{ __('Works & Progress') }}
            </x-jet-responsive-nav-link>
        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="flex items-center px-4">
                @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                    <div class="flex-shrink-0 mr-3">
                        <img class="h-10 w-10 rounded-full object-cover" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                    </div>
                @endif

                <div>
                    <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>
                    <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>
                </div>
            </div>

            <div class="mt-3 space-y-1">
                <!-- Account Management -->
                <x-jet-responsive-nav-link style="text-decoration:none;"  href="{{ route('profile.show') }}" :active="request()->routeIs('profile.show')">
                    {{ __('Profile') }}
                </x-jet-responsive-nav-link>

                @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                    <x-jet-responsive-nav-link style="text-decoration:none;"  href="{{ route('api-tokens.index') }}" :active="request()->routeIs('api-tokens.index')">
                        {{ __('API Tokens') }}
                    </x-jet-responsive-nav-link>
                @endif

                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-jet-responsive-nav-link style="text-decoration:none;"  href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                    this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-jet-responsive-nav-link>
                </form>

                <!-- Team Management -->
                @if (Laravel\Jetstream\Jetstream::hasTeamFeatures())
                    <div class="border-t border-gray-200"></div>

                    <div   class="block px-4 py-2 text-xs text-gray-400">
                        {{ __('Manage Team') }}
                    </div>

                    <!-- Team Settings -->
                    <x-jet-responsive-nav-link style="text-decoration:none;"  href="{{ route('teams.show', Auth::user()->currentTeam->id) }}" :active="request()->routeIs('teams.show')">
                        {{ __('Team Settings') }}
                    </x-jet-responsive-nav-link>

                    @can('create', Laravel\Jetstream\Jetstream::newTeamModel())
                        <x-jet-responsive-nav-link style="text-decoration:none;"  href="{{ route('teams.create') }}" :active="request()->routeIs('teams.create')">
                            {{ __('Create New Team') }}
                        </x-jet-responsive-nav-link>
                    @endcan

                    <div class="border-t border-gray-200"></div>

                    <!-- Team Switcher -->
                    <div class="block px-4 py-2 text-xs text-gray-400">
                        {{ __('Switch Teams') }}
                    </div>

                    @foreach (Auth::user()->allTeams() as $team)
                        <x-jet-switchable-team style="text-decoration:none;"  :team="$team" component="jet-responsive-nav-link" />
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</nav>
