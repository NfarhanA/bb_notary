<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <style media="screen">
                #opening {
                  background-image: url("/images/Headline_Letter.png");
                }
              </style>
               <div class="p-6 sm:px-20 border-b border-gray-200" id="opening">
                  <div>
                      <!-- <x-jet-application-logo class="block h-12 w-auto" /> -->
                  </div>

                  <div class="mt-8 text-2xl" id="opening2">
                    Mails And Letters!
                  </div>

                  <div class="mt-6 text-gray-500">

                  </div>
              </div>

              <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.bootstrap4.min.css">
              <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">


              <!-- Content -->
              <!-- button add surat -->
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add_letter" data-bs-whatever="Add Letter">
                <div class="icons">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                  </svg>
                </div>
                <div class="texts">
                  Add
                </div>
              </button>
              <!-- button add surat -->
              <br>
              <br>
              <!-- modal add surat -->
              <div class="modal fade" id="add_letter" tabindex="-1" aria-labelledby="add_clientLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="add_clientLabel" value="">Add Letter</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <!-- Row 0 -->
                        <!-- <div class="btn-group">
                          <a id="pformbtn" onclick="personalForm()" class="btn btn-info active" aria-current="page">Personal</a>
                          <a id="cformbtn"onclick="companyForm()" class="btn btn-info">Company</a>
                        </div>
                        <hr> -->
                        <!-- Row 1 -->
                        <div class="row g-3">
                          <div class="col">
                        <div class="mb-3">
                          <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hash" viewBox="0 0 16 16">
                              <path d="M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"/>
                            </svg>
                          </div>
                          <div class="texts">
                            <label for="recipient-name" class="col-form-label">Nomor Surat</label>
                          </div>
                          <!--  -->
                          <input type="text" class="form-control" id="recipient-name" >
                        </div>
                        </div>
                          <!-- <div class="col">
                            <div class="icons">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bank2" viewBox="0 0 16 16">
                                <path d="M8.277.084a.5.5 0 0 0-.554 0l-7.5 5A.5.5 0 0 0 .5 6h1.875v7H1.5a.5.5 0 0 0 0 1h13a.5.5 0 1 0 0-1h-.875V6H15.5a.5.5 0 0 0 .277-.916l-7.5-5zM12.375 6v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zM8 4a1 1 0 1 1 0-2 1 1 0 0 1 0 2zM.5 15a.5.5 0 0 0 0 1h15a.5.5 0 1 0 0-1H.5z"/>
                              </svg>
                            </div>
                            <div class="texts">
                              <label for="recipient-company" class="col-form-label">Company</label>
                            </div>
                            <input type="text" class="form-control" id="recipient-company">
                        </div> -->
                        </div>
                        <!-- Row 2 -->
                        <div class="row g-3">
                          <div class="col">
                            <div class="icons">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gender-ambiguous" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M11.5 1a.5.5 0 0 1 0-1h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V1.707l-3.45 3.45A4 4 0 0 1 8.5 10.97V13H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V14H6a.5.5 0 0 1 0-1h1.5v-2.03a4 4 0 1 1 3.471-6.648L14.293 1H11.5zm-.997 4.346a3 3 0 1 0-5.006 3.309 3 3 0 0 0 5.006-3.31z"/>
                              </svg>
                            </div>
                            <div class="texts">
                              <label for="recipient-company" class="col-form-label">Jenis Surat</label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                              <label class="form-check-label" for="flexRadioDefault1">
                                Masuk
                              </label>
                              &nbsp;
                              &nbsp;
                              &nbsp;
                              &nbsp;
                            <!-- </div>
                            <div class="form-check"> -->
                              <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                              <label class="form-check-label" for="flexRadioDefault2">
                                Keluar
                              </label>
                            </div>
                          </div>
                        </div>
                        <br>
                        <!-- Row 3 -->
                        <div class="mb-3">
                          <label for="formFileMultiple" class="form-label">File</label>
                          <input class="form-control" type="file" id="formFileMultiple" multiple>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Archive Table -->
              <table id="example" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                      <tr>
                        <th>No. Surat</th>
                        <th>Type</th>
                        <th>Created At</th>
                        <th>Last Update</th>
                        <th>Action</th>
                      </tr>
                  </thead>
			            <tbody >
                    <?php
                  $i=1
                  ?>
                  <!-- loop -->
                  @foreach($k_letter as $letter)
                    <tr>
                      <td>{{$letter -> no_surat}}</td>
                      <td>{{$letter -> type}}</td>
                      <td>{{$letter -> created_at}}</td>
                      <td>{{$letter -> updated_at}}</td>
                      <td><button type="button" class="btn btn-info">View</button></td>
                    </tr>
                    <?php
                    $i=+1
                     ?>
                     @endforeach
                  </tbody>
			                 <tfoot>
                          <tr>
                            <th>No. Surat</th>
                            <th>Type</th>
                            <th>Created At</th>
                            <th>Last Update</th>
                            <th>Action</th>
                          </tr>
                        </tfoot>
                  </table>


              <!-- JavaScript Bundle with Popper -->
              <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.bootstrap4.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.colVis.min.js"></script>
              <script type="text/javascript">
              $('#myModal').on('shown.bs.modal', function () {
                $('#myInput').trigger('focus')
              })

              $(document).ready(function() {
                var table = $('#example').DataTable( {
                    lengthChange: false,
                    buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                } );

                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
              } );

                function toggleFormOn() {
                  document.getElementById("form").style.display = "block";
                  document.getElementById("btnO").style.display = "none";
                  document.getElementById("btnC").style.display = "block";
                }
                function toggleFormOff() {
                  document.getElementById("form").style.display = "none";
                  document.getElementById("btnO").style.display = "block";
                  document.getElementById("btnC").style.display = "none";
                }

              </script>
              <!-- js add letter -->
              <script type="text/javascript">
              var exampleModal = document.getElementById('works_detail')
              exampleModal.addEventListener('show.bs.modal', function (event) {
                // Button that triggered the modal
                var button = event.relatedTarget
                // Extract info from data-bs-* attributes
                var recipient = button.getAttribute('data-bs-whatever')
                // If necessary, you could initiate an AJAX request here
                // and then do the updating in a callback.
                //
                // Update the modal's content.
                var modalTitle = exampleModal.querySelector('.modal-title')
                var modalBodyInput = exampleModal.querySelector('.modal-body input')

                modalTitle.textContent = recipient
                modalBodyInput.value = recipient
              })
              </script>

            </div>
        </div>
    </div>
</x-app-layout>
