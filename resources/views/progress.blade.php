 <x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <!-- 3rd party css -->
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.bootstrap4.min.css">
              <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
              <!-- 3rd party css -->

              <!-- Content Header -->
              <style media="screen">
                #opening {
                  background-image: url("/images/Headline_Progress.png");
                }
              </style>
               <div class="p-6 sm:px-20 border-b border-gray-200" id="opening">
                  <div>
                      <!-- <x-jet-application-logo class="block h-12 w-auto" /> -->
                  </div>

                  <div class="mt-8 text-2xl" id="opening2">
                      Check Your Work Progresses!
                  </div>

                  <div class="mt-6 text-gray-500">

                  </div>
              </div>
              <!-- Content Header -->

              <!-- Content Body -->
              <div class="p-6 sm:px-20 bg-white border-b border-gray-200">

              <div class="d-grid gap-2 d-md-block">
              <div class="btn-group" role="group" aria-label="Basic example">
              <!-- Add Client Button -->
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add_client" data-bs-whatever="Add New Client">
                <div class="icons">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16">
                    <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                    <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                  </svg>
                </div>
                <div class="texts">
                  Add Client
                </div>
              </button>
              <br>
              <br>
              <!-- Add Client Button -->

              <!-- Add Work Button -->
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add_work" data-bs-whatever="Add New Work">
                <div class="icons">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-brush" viewBox="0 0 16 16">
                    <path d="M15.825.12a.5.5 0 0 1 .132.584c-1.53 3.43-4.743 8.17-7.095 10.64a6.067 6.067 0 0 1-2.373 1.534c-.018.227-.06.538-.16.868-.201.659-.667 1.479-1.708 1.74a8.118 8.118 0 0 1-3.078.132 3.659 3.659 0 0 1-.562-.135 1.382 1.382 0 0 1-.466-.247.714.714 0 0 1-.204-.288.622.622 0 0 1 .004-.443c.095-.245.316-.38.461-.452.394-.197.625-.453.867-.826.095-.144.184-.297.287-.472l.117-.198c.151-.255.326-.54.546-.848.528-.739 1.201-.925 1.746-.896.126.007.243.025.348.048.062-.172.142-.38.238-.608.261-.619.658-1.419 1.187-2.069 2.176-2.67 6.18-6.206 9.117-8.104a.5.5 0 0 1 .596.04zM4.705 11.912a1.23 1.23 0 0 0-.419-.1c-.246-.013-.573.05-.879.479-.197.275-.355.532-.5.777l-.105.177c-.106.181-.213.362-.32.528a3.39 3.39 0 0 1-.76.861c.69.112 1.736.111 2.657-.12.559-.139.843-.569.993-1.06a3.122 3.122 0 0 0 .126-.75l-.793-.792zm1.44.026c.12-.04.277-.1.458-.183a5.068 5.068 0 0 0 1.535-1.1c1.9-1.996 4.412-5.57 6.052-8.631-2.59 1.927-5.566 4.66-7.302 6.792-.442.543-.795 1.243-1.042 1.826-.121.288-.214.54-.275.72v.001l.575.575zm-4.973 3.04.007-.005a.031.031 0 0 1-.007.004zm3.582-3.043.002.001h-.002z"/>
                  </svg>
                </div>
                <div class="texts">
                  Add Work
                </div>
              </button>
              <br>
              <br>
              <!-- Add Work Button -->

              <!-- History Button -->
              <button type="button" class="btn btn-primary">
                <div class="icons">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hourglass-top" viewBox="0 0 16 16">
                    <path d="M2 14.5a.5.5 0 0 0 .5.5h11a.5.5 0 1 0 0-1h-1v-1a4.5 4.5 0 0 0-2.557-4.06c-.29-.139-.443-.377-.443-.59v-.7c0-.213.154-.451.443-.59A4.5 4.5 0 0 0 12.5 3V2h1a.5.5 0 0 0 0-1h-11a.5.5 0 0 0 0 1h1v1a4.5 4.5 0 0 0 2.557 4.06c.29.139.443.377.443.59v.7c0 .213-.154.451-.443.59A4.5 4.5 0 0 0 3.5 13v1h-1a.5.5 0 0 0-.5.5zm2.5-.5v-1a3.5 3.5 0 0 1 1.989-3.158c.533-.256 1.011-.79 1.011-1.491v-.702s.18.101.5.101.5-.1.5-.1v.7c0 .701.478 1.236 1.011 1.492A3.5 3.5 0 0 1 11.5 13v1h-7z"/>
                  </svg>
                </div>
                <div class="texts">
                  History
                </div>
              </button>
              <!-- History Button -->
            </div>
            </div>


              <!-- Add Client Full screen modal -->
              <div class="modal fade" id="add_client" tabindex="-1" aria-labelledby="add_clientLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="add_clientLabel" value="">Add a New Client</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <!-- Row 0 -->
                        <!-- <div class="btn-group">
                          <a id="pformbtn" onclick="personalForm()" class="btn btn-info active" aria-current="page">Personal</a>
                          <a id="cformbtn"onclick="companyForm()" class="btn btn-info">Company</a>
                        </div>
                        <hr> -->
                        <!-- Row 1 -->
                        <div class="row g-3">
                          <div class="col">
                        <div class="mb-3">
                          <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hash" viewBox="0 0 16 16">
                              <path d="M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"/>
                            </svg>
                          </div>
                          <div class="texts">
                            <label for="recipient-name" class="col-form-label">Nama</label>
                          </div>
                          <!--  -->
                          <input type="text" class="form-control" id="recipient-name" >
                        </div>
                        </div>
                          <!-- <div class="col">
                            <div class="icons">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bank2" viewBox="0 0 16 16">
                                <path d="M8.277.084a.5.5 0 0 0-.554 0l-7.5 5A.5.5 0 0 0 .5 6h1.875v7H1.5a.5.5 0 0 0 0 1h13a.5.5 0 1 0 0-1h-.875V6H15.5a.5.5 0 0 0 .277-.916l-7.5-5zM12.375 6v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zM8 4a1 1 0 1 1 0-2 1 1 0 0 1 0 2zM.5 15a.5.5 0 0 0 0 1h15a.5.5 0 1 0 0-1H.5z"/>
                              </svg>
                            </div>
                            <div class="texts">
                              <label for="recipient-company" class="col-form-label">Company</label>
                            </div>
                            <input type="text" class="form-control" id="recipient-company">
                        </div> -->
                        </div>
                        <!-- Row 2 -->
                        <div class="row g-3">
                          <div class="col">
                        <div class="mb-3">
                          <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-medical" viewBox="0 0 16 16">
                              <path d="M7.5 5.5a.5.5 0 0 0-1 0v.634l-.549-.317a.5.5 0 1 0-.5.866L6 7l-.549.317a.5.5 0 1 0 .5.866l.549-.317V8.5a.5.5 0 1 0 1 0v-.634l.549.317a.5.5 0 1 0 .5-.866L8 7l.549-.317a.5.5 0 1 0-.5-.866l-.549.317V5.5zm-2 4.5a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zm0 2a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5z"/>
                              <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5v2z"/>
                            </svg>
                          </div>
                          <div class="texts">
                            <label for="recipient-name" class="col-form-label">No. Kartu Keluarga</label>
                          </div>
                          <input type="text" class="form-control" id="recipient-name">
                        </div>
                        </div>
                          <div class="col">
                            <div class="icons">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-credit-card-2-front" viewBox="0 0 16 16">
                                <path d="M14 3a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h12zM2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H2z"/>
                                <path d="M2 5.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-1zm0 3a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5z"/>
                              </svg>
                            </div>
                            <div class="texts">
                              <label for="recipient-company" class="col-form-label">NIK</label>
                            </div>
                            <input type="text" class="form-control" id="recipient-company">
                        </div>
                        </div>
                        <!-- Row 3 -->
                        <div class="row g-3">
                          <div class="col">
                        <div class="mb-3">
                          <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-event" viewBox="0 0 16 16">
                              <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z"/>
                              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                            </svg>
                          </div>
                          <div class="texts">
                            <label for="recipient-name" class="col-form-label">Tempat/Tanggal Lahir</label>
                          </div>
                          <div class="form-group row">
                            <div class="col-10">
                              <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
                            </div>
                          </div>
                        </div>
                        </div>
                          <div class="col">
                            <div class="icons">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gender-ambiguous" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M11.5 1a.5.5 0 0 1 0-1h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V1.707l-3.45 3.45A4 4 0 0 1 8.5 10.97V13H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V14H6a.5.5 0 0 1 0-1h1.5v-2.03a4 4 0 1 1 3.471-6.648L14.293 1H11.5zm-.997 4.346a3 3 0 1 0-5.006 3.309 3 3 0 0 0 5.006-3.31z"/>
                              </svg>
                            </div>
                            <div class="texts">
                              <label for="recipient-company" class="col-form-label">Jenis Kelamin</label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                              <label class="form-check-label" for="flexRadioDefault1">
                                Male
                              </label>
                              &nbsp;
                              &nbsp;
                              &nbsp;
                              &nbsp;
                            <!-- </div>
                            <div class="form-check"> -->
                              <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                              <label class="form-check-label" for="flexRadioDefault2">
                                Female
                              </label>
                            </div>
                        </div>
                        </div>
                        <!-- Row 4 -->
                        <div class="icons">
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pin-map-fill" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M3.1 11.2a.5.5 0 0 1 .4-.2H6a.5.5 0 0 1 0 1H3.75L1.5 15h13l-2.25-3H10a.5.5 0 0 1 0-1h2.5a.5.5 0 0 1 .4.2l3 4a.5.5 0 0 1-.4.8H.5a.5.5 0 0 1-.4-.8l3-4z"/>
                            <path fill-rule="evenodd" d="M4 4a4 4 0 1 1 4.5 3.969V13.5a.5.5 0 0 1-1 0V7.97A4 4 0 0 1 4 3.999z"/>
                          </svg>
                        </div>
                        <div class="texts">
                          <label for="message-text" class="col-form-label">Alamat</label>
                        </div>
                        <div class="row g-3">
                          <div class="col">
                            <div class="mb-3">
                              <textarea class="form-control" id="message-text" rows="4" cols="10"></textarea>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="icons">
                              RT
                              &nbsp;
                            </div>
                            <div class="texts">
                              <input type="number" class="form-control" id="recipient-company" min="1" max="100">
                            </div>
                            <br>
                            <br>
                            <div class="icons">
                              RW
                            </div>
                            <div class="texts">
                              <input type="number" class="form-control" id="recipient-company" min="1" max="100">
                            </div>
                          </div>
                          <!--  -->
                          <div class="col">
                            <div class="icons">
                              Kec.
                            </div>
                            <div class="texts">
                              <input type="text" class="form-control" id="recipient-company">
                            </div>
                            <br>
                            <br>
                            <div class="icons">
                              Kel.
                            </div>
                            <div class="texts">
                              <input type="text" class="form-control" id="recipient-company">
                            </div>
                          </div>
                        </div>
                        <!-- Row 5 -->
                        <div class="row g-3">
                          <div class="col">
                        <div class="mb-3">
                          <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
                              <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                              <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
                              <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
                            </svg>
                          </div>
                          <div class="texts">
                            <label for="recipient-name" class="col-form-label" >Status Perkawinan</label>
                          </div>
                          <input type="hidden" class="form-control" id="recipient-name">
                          <select class="form-select" aria-label="Default select example">
                            <option selected>Status</option>
                            <option value="1">Belum Kawin</option>
                            <option value="2">Kawin</option>
                            <option value="3">Cerai Hidup</option>
                            <option value="4">Cerai Mati</option>
                          </select>
                        </div>
                        </div>
                          <div class="col">
                            <div class="icons">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-shop" viewBox="0 0 16 16">
                                <path d="M2.97 1.35A1 1 0 0 1 3.73 1h8.54a1 1 0 0 1 .76.35l2.609 3.044A1.5 1.5 0 0 1 16 5.37v.255a2.375 2.375 0 0 1-4.25 1.458A2.371 2.371 0 0 1 9.875 8 2.37 2.37 0 0 1 8 7.083 2.37 2.37 0 0 1 6.125 8a2.37 2.37 0 0 1-1.875-.917A2.375 2.375 0 0 1 0 5.625V5.37a1.5 1.5 0 0 1 .361-.976l2.61-3.045zm1.78 4.275a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 1 0 2.75 0V5.37a.5.5 0 0 0-.12-.325L12.27 2H3.73L1.12 5.045A.5.5 0 0 0 1 5.37v.255a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0zM1.5 8.5A.5.5 0 0 1 2 9v6h1v-5a1 1 0 0 1 1-1h3a1 1 0 0 1 1 1v5h6V9a.5.5 0 0 1 1 0v6h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V9a.5.5 0 0 1 .5-.5zM4 15h3v-5H4v5zm5-5a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1v-3zm3 0h-2v3h2v-3z"/>
                              </svg>
                            </div>
                            <div class="texts">
                              <label for="recipient-company" class="col-form-label">Pekerjaan</label>
                            </div>
                            <input type="text" class="form-control" id="recipient-company">
                        </div>
                        </div>
                        <br>
                        <br>
                        <!-- row 6 -->
                        <label for="formFileMultiple" class="form-label">Files</label>
                        <hr>
                        <div class="mb-3">
                          <label for="formFileMultiple" class="form-label">KTP</label>
                          <input class="form-control" type="file" id="formFileMultiple" multiple>
                        </div>
                        <div class="mb-3">
                          <label for="formFileMultiple" class="form-label">KK</label>
                          <input class="form-control" type="file" id="formFileMultiple" multiple>
                        </div>
                        <div class="mb-3">
                          <label for="formFileMultiple" class="form-label">NPWP</label>
                          <input class="form-control" type="file" id="formFileMultiple" multiple>
                        </div>
                        <div class="mb-3">
                          <label for="formFileMultiple" class="form-label">Sertifikat</label>
                          <input class="form-control" type="file" id="formFileMultiple" multiple>
                        </div>
                        <div class="mb-3">
                          <label for="formFileMultiple" class="form-label">Buku Nikah</label>
                          <input class="form-control" type="file" id="formFileMultiple" multiple>
                        </div>
                        <div class="mb-3">
                          <label for="formFileMultiple" class="form-label">PBB</label>
                          <input class="form-control" type="file" id="formFileMultiple" multiple>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Add Client Full screen modal -->

              <!-- Add Work Full screen modal -->
              <div class="modal fade" id="add_work" tabindex="-1" aria-labelledby="add_clientLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="add_clientLabel" value="">Add a New Work</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <!-- Row 1 -->
                        <div class="row g-3">
                          <div class="col">
                        <div class="mb-3">
                          <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hash" viewBox="0 0 16 16">
                              <path d="M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"/>
                            </svg>
                          </div>
                          <div class="texts">
                            <label for="recipient-name" class="col-form-label">Works Name</label>
                          </div>
                          <!--  -->
                          <input type="text" class="form-control" id="recipient-name" >
                        </div>
                        </div>
                        </div>
                        <!-- Row 2 -->
                        <div class="row g-3">
                          <div class="col">
                            <div class="icons">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
                                <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
                                <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
                              </svg>
                            </div>
                            <div class="texts">
                              <label for="recipient-company" class="col-form-label">Clients</label>
                            </div>
                            <br>
                            <div class="btn-group" role="group" aria-label="Basic example">
                              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#client_list" data-bs-whatever="Add New Work">
                                Add Clients
                              </button>
                              <button type="button" class="btn btn-success">Middle</button>
                            </div>
                        </div>
                        </div>
                        <!-- Row 3 -->
                        <div class="row g-3">
                          <div class="col">
                        <div class="mb-3">
                          <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-event" viewBox="0 0 16 16">
                              <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z"/>
                              <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                            </svg>
                          </div>
                          <div class="texts">
                            <label for="recipient-name" class="col-form-label">Tanggal Mulai</label>
                          </div>
                          <div class="form-group row">
                            <div class="col-10">
                              <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
                            </div>
                          </div>
                        </div>
                        </div>
                          <div class="col">
                        <div class="mb-3">
                          <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                              <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                              <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                            </svg>
                          </div>
                          <div class="texts">
                            <label for="recipient-name" class="col-form-label">Tanggal Selesai</label>
                          </div>
                          <div class="form-group row">
                            <div class="col-10">
                              <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
                            </div>
                          </div>
                        </div>
                        </div>
                        </div>
                        <!-- Row 4 -->
                        <div class="icons">
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-card-heading" viewBox="0 0 16 16">
                            <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
                            <path d="M3 8.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0-5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-1z"/>
                          </svg>
                        </div>
                        <div class="texts">
                          <label for="message-text" class="col-form-label">Notes</label>
                        </div>
                        <div class="row g-3">
                          <div class="col">
                            <div class="mb-3">
                              <textarea class="form-control" id="message-text" rows="4" cols="10"></textarea>
                            </div>
                          </div>
                        </div>
                        <!-- Row 5 -->
                        <div class="row g-3">
                          <div class="col">
                            <div class="icons">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-folder-check" viewBox="0 0 16 16">
                                <path d="m.5 3 .04.87a1.99 1.99 0 0 0-.342 1.311l.637 7A2 2 0 0 0 2.826 14H9v-1H2.826a1 1 0 0 1-.995-.91l-.637-7A1 1 0 0 1 2.19 4h11.62a1 1 0 0 1 .996 1.09L14.54 8h1.005l.256-2.819A2 2 0 0 0 13.81 3H9.828a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 6.172 1H2.5a2 2 0 0 0-2 2zm5.672-1a1 1 0 0 1 .707.293L7.586 3H2.19c-.24 0-.47.042-.683.12L1.5 2.98a1 1 0 0 1 1-.98h3.672z"/>
                                <path d="M15.854 10.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.707 0l-1.5-1.5a.5.5 0 0 1 .707-.708l1.146 1.147 2.646-2.647a.5.5 0 0 1 .708 0z"/>
                              </svg>
                            </div>
                            <div class="texts">
                              <label for="recipient-company" class="col-form-label">Output</label>
                            </div>
                            <input type="text" class="form-control" id="recipient-company">
                        </div>
                        </div>
                        <br>
                        <br>
                        <!-- row 6 -->
                        <!-- Milestone
                        <hr>
                        Time
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                        </div> -->
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Add Client Full screen modal -->

              <!-- Add Work Full screen modal -->
              <div class="modal fade" id="client_list" tabindex="-1" aria-labelledby="add_clientLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="add_clientLabel" value="">Clients List</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Add Client Full screen modal -->
              <br>
              <hr>
              <br>
              <!-- Works Table -->
              <h3>On-Progress Works</h3>
              <table id="example" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                      <tr>
                        <th>No. Works</th>
                        <th>Nama</th>
                        <th>Clients</th>
                        <th>Start</th>
                        <th>Finish</th>
                        <th>Output</th>
                        <th>Last Update</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody >
                      <tr>
                        <td>w0000001</td>
                        <td>Pembuatan CV</td>
                        <td>A, B ,C</td>

                        <td>2011/04/25</td>
                        <td>2011/04/25</td>

                        <td>CV</td>
                        <td>2011/04/25</td>

                        <td>
                          <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#works_detail" data-bs-whatever="Pembuatan CV">
                            Detail
                          </button>
                        </td>
                      </tr>


                      </tbody>
                      <tfoot>
                          <tr>
                            <th>No. Works</th>
                            <th>Nama</th>
                            <th>Clients</th>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Output</th>
                            <th>Last Update</th>
                            <th>Action</th>
                          </tr>
                      </tfoot>
                  </table>
                  <!-- Works Table -->
                  <!-- Work Full screen modal -->
                  <div class="modal fade" id="works_detail" tabindex="-1" aria-labelledby="add_clientLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="works_detail" value=""></h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                          <form>
                            <!-- Row 1 -->
                            <div class="row g-3">
                              <div class="col">
                            <div class="mb-3">
                              <div class="icons">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hash" viewBox="0 0 16 16">
                                  <path d="M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"/>
                                </svg>
                              </div>
                              <div class="texts">
                                <label for="recipient-name" class="col-form-label">Works Name</label>
                              </div>
                              <!--  -->
                              <input type="text" class="form-control" id="recipient-name" >
                            </div>
                            </div>
                            </div>
                            <!-- Row 2 -->
                            <div class="row g-3">
                              <div class="col">
                                <div class="icons">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
                                    <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                    <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
                                    <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
                                  </svg>
                                </div>
                                <div class="texts">
                                  <label for="recipient-company" class="col-form-label">Clients</label>
                                </div>
                                <br>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                  <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#client_list" data-bs-whatever="Add New Work">
                                    Add Clients
                                  </button>
                                  <button type="button" class="btn btn-success">Middle</button>
                                </div>
                            </div>
                            </div>
                            <!-- Row 3 -->
                            <div class="row g-3">
                              <div class="col">
                            <div class="mb-3">
                              <div class="icons">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-event" viewBox="0 0 16 16">
                                  <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z"/>
                                  <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                </svg>
                              </div>
                              <div class="texts">
                                <label for="recipient-name" class="col-form-label">Tanggal Mulai</label>
                              </div>
                              <div class="form-group row">
                                <div class="col-10">
                                  <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
                                </div>
                              </div>
                            </div>
                            </div>
                              <div class="col">
                            <div class="mb-3">
                              <div class="icons">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                                  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                  <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                                </svg>
                              </div>
                              <div class="texts">
                                <label for="recipient-name" class="col-form-label">Tanggal Selesai</label>
                              </div>
                              <div class="form-group row">
                                <div class="col-10">
                                  <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
                                </div>
                              </div>
                            </div>
                            </div>
                            </div>
                            <!-- Row 4 -->
                            <div class="icons">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-card-heading" viewBox="0 0 16 16">
                                <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
                                <path d="M3 8.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0-5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-1z"/>
                              </svg>
                            </div>
                            <div class="texts">
                              <label for="message-text" class="col-form-label">Notes</label>
                            </div>
                            <div class="row g-3">
                              <div class="col">
                                <div class="mb-3">
                                  <textarea class="form-control" id="message-text" rows="4" cols="10"></textarea>
                                </div>
                              </div>
                            </div>
                            <!-- Row 5 -->
                            <div class="row g-3">
                              <div class="col">
                                <div class="icons">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-folder-check" viewBox="0 0 16 16">
                                    <path d="m.5 3 .04.87a1.99 1.99 0 0 0-.342 1.311l.637 7A2 2 0 0 0 2.826 14H9v-1H2.826a1 1 0 0 1-.995-.91l-.637-7A1 1 0 0 1 2.19 4h11.62a1 1 0 0 1 .996 1.09L14.54 8h1.005l.256-2.819A2 2 0 0 0 13.81 3H9.828a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 6.172 1H2.5a2 2 0 0 0-2 2zm5.672-1a1 1 0 0 1 .707.293L7.586 3H2.19c-.24 0-.47.042-.683.12L1.5 2.98a1 1 0 0 1 1-.98h3.672z"/>
                                    <path d="M15.854 10.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.707 0l-1.5-1.5a.5.5 0 0 1 .707-.708l1.146 1.147 2.646-2.647a.5.5 0 0 1 .708 0z"/>
                                  </svg>
                                </div>
                                <div class="texts">
                                  <label for="recipient-company" class="col-form-label">Output</label>
                                </div>
                                <input type="text" class="form-control" id="recipient-company">
                            </div>
                            </div>
                            <br>
                            <br>
                            <!-- row 6 -->
                            Milestone
                            <hr>
                            Time
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Work Full screen modal -->


              <!-- JavaScript Bundle with Popper -->
              <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.bootstrap4.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.colVis.min.js"></script>
              <!-- JavaScript Bundle with Popper -->

              <!-- JS -->
              <script type="text/javascript">
              var exampleModal = document.getElementById('works_detail')
              exampleModal.addEventListener('show.bs.modal', function (event) {
                // Button that triggered the modal
                var button = event.relatedTarget
                // Extract info from data-bs-* attributes
                var recipient = button.getAttribute('data-bs-whatever')
                // If necessary, you could initiate an AJAX request here
                // and then do the updating in a callback.
                //
                // Update the modal's content.
                var modalTitle = exampleModal.querySelector('.modal-title')
                var modalBodyInput = exampleModal.querySelector('.modal-body input')

                modalTitle.textContent = recipient
                modalBodyInput.value = recipient
              })
              </script>
              <!-- JS -->

            </div>
        </div>
    </div>
</x-app-layout>
