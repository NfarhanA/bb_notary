<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <style media="screen">
                #opening {
                  background-image: url("/images/Artboard_2.png");
                }
              </style>
               <div class="p-6 sm:px-20 border-b border-gray-200" id="opening">
                  <div>
                      <!-- <x-jet-application-logo class="block h-12 w-auto" /> -->
                  </div>

                  <div class="mt-8 text-2xl" id="opening2">
                      Welcome to your Client Data!
                  </div>

                  <div class="mt-6 text-gray-500">

                  </div>
              </div>

              <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.bootstrap4.min.css">
              <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">


              <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                          <tr>
                              <th>Photo</th>
                              <th>Name</th>
                              <th>NIP</th>
                              <th>Last Update</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody >
                          <tr>
                              <td>
                                <!-- <img src="{{asset('/images/220px-Escaping_criticism-by_pere_borrel_del_caso.png')}}" width="100" height="50" /> -->

                                <!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                   viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                                <style type="text/css">
                                  .st0no{fill:none;stroke:#575756;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                                  .st1no{fill:none;stroke:#575756;stroke-width:10;stroke-linejoin:round;stroke-miterlimit:10;}
                                  .st2no{fill:#FFFFFF;stroke:#36A9E1;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                                  .st3no{fill:none;}
                                  .st4no{fill:#575756;}
                                  .st5no{font-family:'Calibri';}
                                  .st6no{font-size:60px;}
                                </style>
                                <rect x="117" y="61.1" class="st0no" width="270" height="225"/>
                                <circle class="st1no" cx="247.6" cy="106.3" r="16.4"/>
                                <polyline class="st0no" points="114.9,225 197.3,138.4 272.3,220.9 "/>
                                <line class="st0no" x1="217.5" y1="286.1" x2="387" y2="84.9"/>
                                <path class="st2no" d="M607.8,349.3"/>
                                <rect x="108.2" y="311.4" class="st3no" width="289" height="127.5"/>
                                <text transform="matrix(1 0 0 1 137.1125 352.1154)"><tspan x="0" y="0" class="st4no st5no st6no">No Image</tspan><tspan x="7" y="72" class="st4 st5 st6"></tspan></text>
                                </svg>
                              </td>
                              <td>Antonio Bernard</td>
                              <td>1298762146</td>

                              <td>2011/04/25</td>
                              <td>
                                <button type="button" class="btn btn-outline-info">Info Detail</button>
                              </td>
                          </tr>
                          <tr>
                              <td align="center">
                                <img src="{{asset('/images/careerstrailerbox.jpg')}}" width="100" height="50" />
                              </td>
                              <td>Anastasya Kova</td>
                              <td>345435345</td>

                              <td>2011/04/25</td>
                              <td>
                                <button type="button" class="btn btn-outline-info">Info Detail</button>
                              </td>
                          </tr>
                          <tr>
                              <td align="center">
                                <img src="{{asset('/images/julia-barretto-profile-picture-idea-1598430021.jpg')}}" width="100" height="50" />
                              </td>
                              <td>Juliana</td>
                              <td>1238976282</td>

                              <td>2011/04/25</td>
                              <td>
                                <button type="button" class="btn btn-outline-info">Info Detail</button>
                              </td>
                          </tr>

                      </tbody>
                      <tfoot>
                          <tr>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>NIP</th>

                            <th>Last Update</th>
                            <th>Action</th>
                          </tr>
                      </tfoot>
                  </table>

                  <style media="screen">
                  .icons,
                  .texts {
                    vertical-align: middle;
                    display: inline-block;
                  }
                  </style>

                  <a href="#form">
                    <button id="btnO" wtype="button" class="btn btn-light" onclick="toggleFormOn()">
                      <span class="icons">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-plus-square-dotted" viewBox="0 0 16 16">
                          <path d="M2.5 0c-.166 0-.33.016-.487.048l.194.98A1.51 1.51 0 0 1 2.5 1h.458V0H2.5zm2.292 0h-.917v1h.917V0zm1.833 0h-.917v1h.917V0zm1.833 0h-.916v1h.916V0zm1.834 0h-.917v1h.917V0zm1.833 0h-.917v1h.917V0zM13.5 0h-.458v1h.458c.1 0 .199.01.293.029l.194-.981A2.51 2.51 0 0 0 13.5 0zm2.079 1.11a2.511 2.511 0 0 0-.69-.689l-.556.831c.164.11.305.251.415.415l.83-.556zM1.11.421a2.511 2.511 0 0 0-.689.69l.831.556c.11-.164.251-.305.415-.415L1.11.422zM16 2.5c0-.166-.016-.33-.048-.487l-.98.194c.018.094.028.192.028.293v.458h1V2.5zM.048 2.013A2.51 2.51 0 0 0 0 2.5v.458h1V2.5c0-.1.01-.199.029-.293l-.981-.194zM0 3.875v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zM0 5.708v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zM0 7.542v.916h1v-.916H0zm15 .916h1v-.916h-1v.916zM0 9.375v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zm-16 .916v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zm-16 .917v.458c0 .166.016.33.048.487l.98-.194A1.51 1.51 0 0 1 1 13.5v-.458H0zm16 .458v-.458h-1v.458c0 .1-.01.199-.029.293l.981.194c.032-.158.048-.32.048-.487zM.421 14.89c.183.272.417.506.69.689l.556-.831a1.51 1.51 0 0 1-.415-.415l-.83.556zm14.469.689c.272-.183.506-.417.689-.69l-.831-.556c-.11.164-.251.305-.415.415l.556.83zm-12.877.373c.158.032.32.048.487.048h.458v-1H2.5c-.1 0-.199-.01-.293-.029l-.194.981zM13.5 16c.166 0 .33-.016.487-.048l-.194-.98A1.51 1.51 0 0 1 13.5 15h-.458v1h.458zm-9.625 0h.917v-1h-.917v1zm1.833 0h.917v-1h-.917v1zm1.834-1v1h.916v-1h-.916zm1.833 1h.917v-1h-.917v1zm1.833 0h.917v-1h-.917v1zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                        </svg>
                      </span>
                      <span class="texts">
                        Add More
                      </span>
                   </button>
                  </a>

                  <button  id="btnC"type="button" class="btn btn-light" onclick="toggleFormOff()"style="display: none;">
                    <span class="icons">
                      <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                      </svg>
                    </span>
                    <span class="texts">
                      Close
                    </span>
                 </button>
               </br>
              </div>

              <div name="form" id="form" style="display: none;" class="p-6 sm:px-20 bg-white border-b border-gray-200" >
              <form>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Name</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                  <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>

                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Age</label>
                  <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                  <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>

                <label for="exampleInputEmail1" class="form-label">Sex</label>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                  <label class="form-check-label" for="exampleRadios1">
                    Male
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                  <label class="form-check-label" for="exampleRadios2">
                    Female
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                  <label class="form-check-label" for="exampleRadios2">
                    Others
                  </label>
                </div>
                </br>

                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Address</label>
                  <input type="password" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="mb-3">
                  <label for="formFile" class="form-label">Photo</label>
                  <input class="form-control" type="file" id="formFile">
                </div>
                <div class="mb-3">
                  <label for="formFileMultiple" class="form-label">Files</label>
                  <input class="form-control" type="file" id="formFileMultiple" multiple>
                </div>

                <button type="submit" class="btn btn-primary">Add</button>
              </form>
              </div>


              <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                  <div>
                  </div>

                  <div class="mt-8 text-2xl">
                      Welcome to your Client Data!
                  </div>

                  <div class="mt-6 text-gray-500">

                  </div>
              </div>

              <div class="p-6 sm:px-20 bg-white border-b border-gray-200">

              <table style="width:50%">
                      <thead>
                          <tr>
                              <td>Photo</td>
                              <td>
                                <img src="{{asset('/images/220px-Escaping_criticism-by_pere_borrel_del_caso.png')}}" width="100" height="50" />
                              </td>
                          </tr>
                          <tr>
                              <td>Name</td>
                              <td>Antonio Bernard</td>
                          </tr>
                          <tr>
                              <td>NIK</td>
                              <td>1231245325</td>
                          </tr>
                          <tr>
                              <td>NIP</td>
                              <td>1231093210</td>
                          </tr>
                          <tr>
                              <td>Address</td>
                              <td>London</td>
                          </tr>
                          <tr>
                              <td>Age</td>
                              <td>22</td>
                          </tr>
                          <tr>
                              <td>Gender</td>
                              <td>Male</td>
                          </tr>
                          <tr>
                              <td>Last Update</td>
                              <td>6 July 2020</td>
                          </tr>
                          <tr>
                              <td>Files</td>
                          </tr>
                        </thead>
                    </table>
                  </br>

                  <table style="width:100%">
                          <thead>
                          <tr>

                              <td>
                                <span class="icons">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   width="26" height="26" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                <style type="text/css">
                  .st0{fill:#FFFFFF;stroke:#000000;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                  .st1{fill:none;stroke:#000000;stroke-width:9;stroke-linejoin:round;stroke-miterlimit:10;}
                  .st2{fill:#FFFFFF;stroke:#36A9E1;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                </style>
                <g id="Layer_2">
                  <rect x="135.13" y="17.79" class="st0" width="300" height="380"/>
                  <rect x="95.87" y="57.83" class="st0" width="300" height="380"/>
                  <line class="st1" x1="355.01" y1="115" x2="134.99" y2="115"/>
                  <line class="st1" x1="355.01" y1="145" x2="134.99" y2="145"/>
                  <line class="st1" x1="355.01" y1="175" x2="134.99" y2="175"/>
                  <line class="st1" x1="355.01" y1="205" x2="134.99" y2="205"/>
                </g>
                <g id="Layer_1">
                  <path class="st2" d="M607.82,349.29"/>
                  <polygon class="st0" points="355.87,478.21 55.87,478.21 55.87,97.45 205.87,97.45 355.87,240.08 	"/>
                </g>
                </svg>
                                </span>
                                <span class="texts">ClientsFiles.PDF</span>

                            </td>
                            <td>
                              <span class="icons">

                              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="16" height="16" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                              <style type="text/css">
                                .st0{fill:#FFFFFF;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                                .st1{fill:none;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                              </style>
                              <circle class="st0" cx="250" cy="250" r="225"/>
                              <line class="st1" x1="250" y1="405" x2="250" y2="95"/>
                              <polyline class="st1" points="375.69,279.31 250,405 130.44,285.44 "/>
                              </svg>
                              </span>
                              <span class="texts">
                              Download
                              </span>
                              &nbsp;
                              &nbsp;

                              <span class="icons">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="20" height="20" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                                <style type="text/css">
                                  .st0{fill:#FFFFFF;stroke:#000000;stroke-width:20;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st1{fill:#FFFFFF;stroke:#000000;stroke-width:10;stroke-linejoin:round;stroke-miterlimit:10;}
                                  .st2{fill:#FFFFFF;}
                                  .st3{fill:#FFFFFF;stroke:#000000;stroke-width:5;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st4{fill:none;stroke:#000000;stroke-width:16;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st5{fill:none;stroke:#000000;stroke-width:10;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                </style>
                                <rect x="130" y="180" class="st0" width="240" height="300"/>
                                <g>

                                    <rect x="242.45" y="88.8" transform="matrix(0.7071 0.7071 -0.7071 0.7071 186.6247 -222.9573)" class="st0" width="240" height="50"/>

                                    <rect x="362.97" y="64.37" transform="matrix(0.7071 0.7071 -0.7071 0.7071 172.8651 -256.1758)" class="st1" width="65.38" height="32.42"/>
                                </g>
                                <g>
                                  <line class="st2" x1="99.19" y1="32.23" x2="203.04" y2="128.93"/>
                                  <g>
                                    <polygon points="106.01,24.91 178.5,102.63 175.06,106.33 92.38,39.55 		"/>
                                    <g>
                                      <polygon points="154.45,117.75 203.04,128.93 188.43,81.26 			"/>
                                    </g>
                                  </g>
                                </g>
                                <path class="st3" d="M174.22,102.09"/>
                                <line class="st4" x1="130" y1="240" x2="370" y2="240"/>
                                <line class="st5" x1="190" y1="240" x2="190" y2="480"/>
                                <line class="st5" x1="250" y1="240" x2="250" y2="480"/>
                                <line class="st5" x1="310" y1="240" x2="310" y2="480"/>
                                </svg>
                              </span>
                              <span class="texts">
                              Delete
                              </span>
                            </td>
                          </tr>

                          <tr>
                            <td>
                                <span class="icons">
                                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                      width="26" height="26" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                                  <style type="text/css">
                                    .st0{fill:#FFFFFF;stroke:#000000;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                                    .st1{fill:#FFFFFF;stroke:#000000;stroke-width:10;stroke-linejoin:round;stroke-miterlimit:10;}
                                  </style>
                                  <rect x="70" y="100" class="st0" width="360" height="300"/>
                                  <circle class="st1" cx="244.09" cy="160.27" r="21.84"/>
                                  <polyline class="st0" points="67.16,318.52 177.05,203.13 277.05,313.02 "/>
                                  <line class="st0" x1="203.98" y1="400" x2="430" y2="131.7"/>
                                  <path class="st0" d="M607.82,349.29"/>
                                  </svg>
                                </span>
                                <span class="texts">ClientsPhotos.PNG</span>
                              </br>
                            </td>
                            <td>
                              <span class="icons">
                              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="16" height="16" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                              <style type="text/css">
                                .st0{fill:#FFFFFF;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                                .st1{fill:none;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                              </style>
                              <circle class="st0" cx="250" cy="250" r="225"/>
                              <line class="st1" x1="250" y1="405" x2="250" y2="95"/>
                              <polyline class="st1" points="375.69,279.31 250,405 130.44,285.44 "/>
                              </svg>
                              </span>
                              <span class="texts">
                              Download
                              </span>
                              &nbsp;
                              &nbsp;
                              <span class="icons">

                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="20" height="20" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                                <style type="text/css">
                                  .st0{fill:#FFFFFF;stroke:#000000;stroke-width:20;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st1{fill:#FFFFFF;stroke:#000000;stroke-width:10;stroke-linejoin:round;stroke-miterlimit:10;}
                                  .st2{fill:#FFFFFF;}
                                  .st3{fill:#FFFFFF;stroke:#000000;stroke-width:5;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st4{fill:none;stroke:#000000;stroke-width:16;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st5{fill:none;stroke:#000000;stroke-width:10;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                </style>
                                <rect x="130" y="180" class="st0" width="240" height="300"/>
                                <g>

                                    <rect x="242.45" y="88.8" transform="matrix(0.7071 0.7071 -0.7071 0.7071 186.6247 -222.9573)" class="st0" width="240" height="50"/>

                                    <rect x="362.97" y="64.37" transform="matrix(0.7071 0.7071 -0.7071 0.7071 172.8651 -256.1758)" class="st1" width="65.38" height="32.42"/>
                                </g>
                                <g>
                                  <line class="st2" x1="99.19" y1="32.23" x2="203.04" y2="128.93"/>
                                  <g>
                                    <polygon points="106.01,24.91 178.5,102.63 175.06,106.33 92.38,39.55 		"/>
                                    <g>
                                      <polygon points="154.45,117.75 203.04,128.93 188.43,81.26 			"/>
                                    </g>
                                  </g>
                                </g>
                                <path class="st3" d="M174.22,102.09"/>
                                <line class="st4" x1="130" y1="240" x2="370" y2="240"/>
                                <line class="st5" x1="190" y1="240" x2="190" y2="480"/>
                                <line class="st5" x1="250" y1="240" x2="250" y2="480"/>
                                <line class="st5" x1="310" y1="240" x2="310" y2="480"/>
                                </svg>
                              </span>
                              <span class="texts">
                              Delete
                              </span>
                            </td>
                            </tr>

                          <tr>

                          <td>

                          </td>
                        </tr>
                      </thead>
                  </table>
                </br>

                  <button type="button" class="btn btn-light">
                    <span class="icons">
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="20" height="20" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
              <style type="text/css">
                .st0{fill:#FFFFFF;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                .st1{fill:none;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                .st2{fill:none;stroke:#000000;stroke-width:20;stroke-miterlimit:10;}
              </style>
              <circle class="st0" cx="249.6" cy="249.6" r="225"/>
              <polyline class="st1" points="110,255 110,345 390,345 390,255 "/>
              <path class="st2" d="M579.8,325.1"/>
              <path class="st2" d="M250,356.2"/>
              <g>
                <g>
                  <line class="st1" x1="250" y1="315" x2="250" y2="172.6"/>
                  <g>
                    <polygon points="324.8,194.5 250,65 175.2,194.5 			"/>
                  </g>
                </g>
              </g>
              </svg>
                    </span>
                    <span class="texts">
                    Upload File
                    </span>
                  </button>
                   <br>
                   <br>
                   <br>
                   <button type="button" class="btn btn-success">
                     <span class="icons">
                     <?xml version="1.0" encoding="utf-8"?>
              <!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
              <svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  width="26" height="26" viewBox="0 0 1417.3 1417.3" style="enable-background:new 0 0 1417.3 1417.3;" xml:space="preserve">
              <style type="text/css">
                .st0e{fill:none;stroke:#000000;stroke-width:60;stroke-miterlimit:10;}
                .st1e{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:30;stroke-miterlimit:10;}
                .st2e{fill:#FFFFFF;}
              </style>
              <path class="st0e" d="M2085,735"/>
              <path class="st1e" d="M997.5,560.6L636,1185.3H371.8V678.4c0,0,19.4-237.7,336.4-235.2C945.8,440.5,997.5,560.6,997.5,560.6z"/>
              <polygon class="st1e" points="1044.3,931.9 897,1185.3 1044.3,1185.3 "/>
              <circle class="st1e" cx="700.6" cy="199.9" r="154.6"/>
              <g>
                <rect x="496" y="788.2" transform="matrix(0.5 -0.866 0.866 0.5 -266.6426 1256.2001)" class="st2e" width="917.1" height="141.7"/>
                <polygon class="st2e" points="651.3,1242.6 651.1,1384.4 774.1,1313.4 	"/>
              </g>
              </svg>
              </span>
                <span class="texts">
                     Edit Profile
                   </span>
                   </button>
              </div>


              <div class="p-6 sm:px-20 bg-white border-b border-gray-200">

              <table style="width:50%">
                      <thead>
                          <tr>
                              <td>Photo</td>
                              <td>
                                <img src="{{asset('/images/220px-Escaping_criticism-by_pere_borrel_del_caso.png')}}" width="100" height="50" />
                                <br>
                              </td>

                          </tr>
                        <br>
                        <br>
                        <br>
                        <form>
                          <tr>
                              <td>Name</td>
                              <td>
                                <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Antonio Bernard">

                              </div>
                            </td>
                          </tr>
                          <tr>
                              <td>NIK</td>
                              <td><div class="form-group">
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="12312235">
                              </div></td>
                          </tr>
                          <tr>
                              <td>NIP</td>
                              <td><div class="form-group">
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="123123312">
                              </div></td>
                          </tr>
                          <tr>
                              <td>Address</td>
                              <td><div class="form-group">
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="London">
                              </div></td>
                          </tr>
                          <tr>
                              <td>Age</td>
                              <td><div class="form-group">
                                <input min=10 max=140 type="number" class="form-control" id="exampleInputPassword1" placeholder="22">
                              </div></td>
                          </tr>
                          <tr>
                              <td>Gender</td>
                              <td>Male</td>
                          </tr>
                          <tr>
                              <td>Last Update</td>
                              <td>6 July 2020</td>
                          </tr>
                          <tr>
                              <td>Files</td>
                          </tr>
                        </thead>
                    </table>


                  </br>

                  <table style="width:100%">
                          <thead>
                          <tr>

                              <td>
                                <span class="icons">
                                  <?xml version="1.0" encoding="utf-8"?>
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   width="26" height="26" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                <style type="text/css">
                  .st0{fill:#FFFFFF;stroke:#000000;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                  .st1{fill:none;stroke:#000000;stroke-width:9;stroke-linejoin:round;stroke-miterlimit:10;}
                  .st2{fill:#FFFFFF;stroke:#36A9E1;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                </style>
                <g id="Layer_2">
                  <rect x="135.13" y="17.79" class="st0" width="300" height="380"/>
                  <rect x="95.87" y="57.83" class="st0" width="300" height="380"/>
                  <line class="st1" x1="355.01" y1="115" x2="134.99" y2="115"/>
                  <line class="st1" x1="355.01" y1="145" x2="134.99" y2="145"/>
                  <line class="st1" x1="355.01" y1="175" x2="134.99" y2="175"/>
                  <line class="st1" x1="355.01" y1="205" x2="134.99" y2="205"/>
                </g>
                <g id="Layer_1">
                  <path class="st2" d="M607.82,349.29"/>
                  <polygon class="st0" points="355.87,478.21 55.87,478.21 55.87,97.45 205.87,97.45 355.87,240.08 	"/>
                </g>
                </svg>
                                </span>
                                <span class="texts">ClientsFiles.PDF</span>

                            </td>
                            <td>
                              <span class="icons">
                                <?xml version="1.0" encoding="utf-8"?>
                              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="16" height="16" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                              <style type="text/css">
                                .st0{fill:#FFFFFF;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                                .st1{fill:none;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                              </style>
                              <circle class="st0" cx="250" cy="250" r="225"/>
                              <line class="st1" x1="250" y1="405" x2="250" y2="95"/>
                              <polyline class="st1" points="375.69,279.31 250,405 130.44,285.44 "/>
                              </svg>
                              </span>
                              <span class="texts">
                              Download
                              </span>
                              &nbsp;
                              &nbsp;

                              <span class="icons">
                                <?xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="20" height="20" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                                <style type="text/css">
                                  .st0{fill:#FFFFFF;stroke:#000000;stroke-width:20;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st1{fill:#FFFFFF;stroke:#000000;stroke-width:10;stroke-linejoin:round;stroke-miterlimit:10;}
                                  .st2{fill:#FFFFFF;}
                                  .st3{fill:#FFFFFF;stroke:#000000;stroke-width:5;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st4{fill:none;stroke:#000000;stroke-width:16;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st5{fill:none;stroke:#000000;stroke-width:10;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                </style>
                                <rect x="130" y="180" class="st0" width="240" height="300"/>
                                <g>

                                    <rect x="242.45" y="88.8" transform="matrix(0.7071 0.7071 -0.7071 0.7071 186.6247 -222.9573)" class="st0" width="240" height="50"/>

                                    <rect x="362.97" y="64.37" transform="matrix(0.7071 0.7071 -0.7071 0.7071 172.8651 -256.1758)" class="st1" width="65.38" height="32.42"/>
                                </g>
                                <g>
                                  <line class="st2" x1="99.19" y1="32.23" x2="203.04" y2="128.93"/>
                                  <g>
                                    <polygon points="106.01,24.91 178.5,102.63 175.06,106.33 92.38,39.55 		"/>
                                    <g>
                                      <polygon points="154.45,117.75 203.04,128.93 188.43,81.26 			"/>
                                    </g>
                                  </g>
                                </g>
                                <path class="st3" d="M174.22,102.09"/>
                                <line class="st4" x1="130" y1="240" x2="370" y2="240"/>
                                <line class="st5" x1="190" y1="240" x2="190" y2="480"/>
                                <line class="st5" x1="250" y1="240" x2="250" y2="480"/>
                                <line class="st5" x1="310" y1="240" x2="310" y2="480"/>
                                </svg>
                              </span>
                              <span class="texts">
                              Delete
                              </span>
                            </td>
                          </tr>

                          <tr>
                            <td>
                                <span class="icons">
                                  <?xml version="1.0" encoding="utf-8"?>
                                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                      width="26" height="26" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                                  <style type="text/css">
                                    .st0{fill:#FFFFFF;stroke:#000000;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                                    .st1{fill:#FFFFFF;stroke:#000000;stroke-width:10;stroke-linejoin:round;stroke-miterlimit:10;}
                                  </style>
                                  <rect x="70" y="100" class="st0" width="360" height="300"/>
                                  <circle class="st1" cx="244.09" cy="160.27" r="21.84"/>
                                  <polyline class="st0" points="67.16,318.52 177.05,203.13 277.05,313.02 "/>
                                  <line class="st0" x1="203.98" y1="400" x2="430" y2="131.7"/>
                                  <path class="st0" d="M607.82,349.29"/>
                                  </svg>
                                </span>
                                <span class="texts">ClientsPhotos.PNG</span>
                              </br>
                            </td>
                            <td>
                              <span class="icons">
                                <?xml version="1.0" encoding="utf-8"?>
                              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="16" height="16" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                              <style type="text/css">
                                .st0{fill:#FFFFFF;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                                .st1{fill:none;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                              </style>
                              <circle class="st0" cx="250" cy="250" r="225"/>
                              <line class="st1" x1="250" y1="405" x2="250" y2="95"/>
                              <polyline class="st1" points="375.69,279.31 250,405 130.44,285.44 "/>
                              </svg>
                              </span>
                              <span class="texts">
                              Download
                              </span>
                              &nbsp;
                              &nbsp;
                              <span class="icons">
                                <?xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    width="20" height="20" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                                <style type="text/css">
                                  .st0{fill:#FFFFFF;stroke:#000000;stroke-width:20;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st1{fill:#FFFFFF;stroke:#000000;stroke-width:10;stroke-linejoin:round;stroke-miterlimit:10;}
                                  .st2{fill:#FFFFFF;}
                                  .st3{fill:#FFFFFF;stroke:#000000;stroke-width:5;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st4{fill:none;stroke:#000000;stroke-width:16;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                  .st5{fill:none;stroke:#000000;stroke-width:10;stroke-linejoin:bevel;stroke-miterlimit:10;}
                                </style>
                                <rect x="130" y="180" class="st0" width="240" height="300"/>
                                <g>

                                    <rect x="242.45" y="88.8" transform="matrix(0.7071 0.7071 -0.7071 0.7071 186.6247 -222.9573)" class="st0" width="240" height="50"/>

                                    <rect x="362.97" y="64.37" transform="matrix(0.7071 0.7071 -0.7071 0.7071 172.8651 -256.1758)" class="st1" width="65.38" height="32.42"/>
                                </g>
                                <g>
                                  <line class="st2" x1="99.19" y1="32.23" x2="203.04" y2="128.93"/>
                                  <g>
                                    <polygon points="106.01,24.91 178.5,102.63 175.06,106.33 92.38,39.55 		"/>
                                    <g>
                                      <polygon points="154.45,117.75 203.04,128.93 188.43,81.26 			"/>
                                    </g>
                                  </g>
                                </g>
                                <path class="st3" d="M174.22,102.09"/>
                                <line class="st4" x1="130" y1="240" x2="370" y2="240"/>
                                <line class="st5" x1="190" y1="240" x2="190" y2="480"/>
                                <line class="st5" x1="250" y1="240" x2="250" y2="480"/>
                                <line class="st5" x1="310" y1="240" x2="310" y2="480"/>
                                </svg>
                              </span>
                              <span class="texts">
                              Delete
                              </span>
                            </td>
                            </tr>

                          <tr>

                          <td>

                          </td>
                        </tr>
                      </thead>
                  </table>
                </br>

                  <button type="button" class="btn btn-light">
                    <span class="icons">
                      <?xml version="1.0" encoding="utf-8"?>
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="20" height="20" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
              <style type="text/css">
                .st0{fill:#FFFFFF;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                .st1{fill:none;stroke:#000000;stroke-width:30;stroke-miterlimit:10;}
                .st2{fill:none;stroke:#000000;stroke-width:20;stroke-miterlimit:10;}
              </style>
              <circle class="st0" cx="249.6" cy="249.6" r="225"/>
              <polyline class="st1" points="110,255 110,345 390,345 390,255 "/>
              <path class="st2" d="M579.8,325.1"/>
              <path class="st2" d="M250,356.2"/>
              <g>
                <g>
                  <line class="st1" x1="250" y1="315" x2="250" y2="172.6"/>
                  <g>
                    <polygon points="324.8,194.5 250,65 175.2,194.5 			"/>
                  </g>
                </g>
              </g>
              </svg>
                    </span>
                    <span class="texts">
                    Add File
                    </span>
                  </button>
                   </br>
                   </br>
                   </br>
                   <button type="button" class="btn btn-outline-danger">Delete Profile</button>
                   <button type="submit" class="btn btn-primary">
                     <span class="icons">
                       <?xml version="1.0" encoding="utf-8"?>
                        <!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                           width="23" height="23"	 viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                        <style type="text/css">
                          .st0saa{fill:none;stroke:#FFFFFF;stroke-width:20;stroke-miterlimit:10;}
                          .st1saa{fill:none;stroke:#FFFFFF;stroke-width:20;stroke-miterlimit:10;}
                        </style>
                        <polygon class="st0saa" points="430,430 70,430 70,70 292.9,70 329.7,70 430,172.6 "/>
                        <polyline class="st1saa" points="295.2,70 295.2,179.6 141.1,179.6 141.1,70 "/>
                        <polyline class="st1saa" points="116.2,430 116.2,256.8 371.1,256.8 371.1,430 "/>
                        </svg>
                     </span>
                     <span class="texts">
                     Save
                    </span>
                   </button>
               </form>
              </div>
              <!-- <div class="bg-gray-200 bg-opacity-25 grid grid-cols-1 md:grid-cols-2">
                  <div class="p-6">
                      <div class="flex items-center">
                          <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-400"><path d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"></path></svg>
                          <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold"><a href="https://laravel.com/docs">Documentation</a></div>
                      </div>

                      <div class="ml-12">
                          <div class="mt-2 text-sm text-gray-500">
                              Laravel has wonderful documentation covering every aspect of the framework. Whether you're new to the framework or have previous experience, we recommend reading all of the documentation from beginning to end.
                          </div>

                          <a href="https://laravel.com/docs">
                              <div class="mt-3 flex items-center text-sm font-semibold text-indigo-700">
                                      <div>Explore the documentation</div>

                                      <div class="ml-1 text-indigo-500">
                                          <svg viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                                      </div>
                              </div>
                          </a>
                      </div>
                  </div>

                  <div class="p-6 border-t border-gray-200 md:border-t-0 md:border-l">
                      <div class="flex items-center">
                          <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-400"><path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path><path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
                          <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold"><a href="https://laracasts.com">Laracasts</a></div>
                      </div>

                      <div class="ml-12">
                          <div class="mt-2 text-sm text-gray-500">
                              Laracasts offers thousands of video tutorials on Laravel, PHP, and JavaScript development. Check them out, see for yourself, and massively level up your development skills in the process.
                          </div>

                          <a href="https://laracasts.com">
                              <div class="mt-3 flex items-center text-sm font-semibold text-indigo-700">
                                      <div>Start watching Laracasts</div>

                                      <div class="ml-1 text-indigo-500">
                                          <svg viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                                      </div>
                              </div>
                          </a>
                      </div>
                  </div>

                  <div class="p-6 border-t border-gray-200">
                      <div class="flex items-center">
                          <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-400"><path d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>
                          <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold"><a href="https://tailwindcss.com/">Tailwind</a></div>
                      </div>

                      <div class="ml-12">
                          <div class="mt-2 text-sm text-gray-500">
                              Laravel Jetstream is built with Tailwind, an amazing utility first CSS framework that doesn't get in your way. You'll be amazed how easily you can build and maintain fresh, modern designs with this wonderful framework at your fingertips.
                          </div>
                      </div>
                  </div>

                  <div class="p-6 border-t border-gray-200 md:border-l">
                      <div class="flex items-center">
                          <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-400"><path d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path></svg>
                          <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold">Authentication</div>
                      </div>

                      <div class="ml-12">
                          <div class="mt-2 text-sm text-gray-500">
                              Authentication and registration views are included with Laravel Jetstream, as well as support for user email verification and resetting forgotten passwords. So, you're free to get started what matters most: building your application.
                          </div>
                      </div>
                  </div>
              </div> -->



              <!-- JavaScript Bundle with Popper -->
              <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.bootstrap4.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.colVis.min.js"></script>
              <script type="text/javascript">
              $('#myModal').on('shown.bs.modal', function () {
                $('#myInput').trigger('focus')
              })

              $(document).ready(function() {
                var table = $('#example').DataTable( {
                    lengthChange: false,
                    buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                } );

                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
              } );

                function toggleFormOn() {
                  document.getElementById("form").style.display = "block";
                  document.getElementById("btnO").style.display = "none";
                  document.getElementById("btnC").style.display = "block";
                }
                function toggleFormOff() {
                  document.getElementById("form").style.display = "none";
                  document.getElementById("btnO").style.display = "block";
                  document.getElementById("btnC").style.display = "none";
                }

              </script>

            </div>
        </div>
    </div>
</x-app-layout>
