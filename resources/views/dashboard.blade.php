<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <style media="screen">
                #opening {
                  background-image: url("/images/Headline_Dashboard.png");
                }
              </style>
               <div class="p-6 sm:px-20 border-b border-gray-200" id="opening">
                  <div>
                      <!-- <x-jet-application-logo class="block h-12 w-auto" /> -->
                  </div>

                  <div class="mt-8 text-2xl" id="opening2">
                      Welcome to Dwi Notary!
                  </div>

                  <div class="mt-6 text-gray-500">

                  </div>
              </div>

              <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.bootstrap4.min.css">
              <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">



              
              <!-- JavaScript Bundle with Popper -->
              <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.bootstrap4.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.colVis.min.js"></script>
              <script type="text/javascript">
              $('#myModal').on('shown.bs.modal', function () {
                $('#myInput').trigger('focus')
              })

              $(document).ready(function() {
                var table = $('#example').DataTable( {
                    lengthChange: false,
                    buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                } );

                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
              } );

                function toggleFormOn() {
                  document.getElementById("form").style.display = "block";
                  document.getElementById("btnO").style.display = "none";
                  document.getElementById("btnC").style.display = "block";
                }
                function toggleFormOff() {
                  document.getElementById("form").style.display = "none";
                  document.getElementById("btnO").style.display = "block";
                  document.getElementById("btnC").style.display = "none";
                }

              </script>

            </div>
        </div>
    </div>
</x-app-layout>
