<x-app-layout>
  <!-- Header -->
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
  <!-- Header -->

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <!-- Page Title -->
              <!-- Background -->
              <style media="screen">
                #opening {
                  background-image: url("/images/Headline_archive.png");
                }
              </style>
              <!-- Text -->
               <div class="p-6 sm:px-20 border-b border-gray-200" id="opening">
                  <div>
                      <!-- <x-jet-application-logo class="block h-12 w-auto" /> -->
                  </div>

                  <div class="mt-8 text-2xl" id="opening2">
                      Access Your Client Document!
                  </div>

                  <div class="mt-6 text-gray-500">

                  </div>
              </div>
              <!-- Page Title -->

              <!-- Style -->
              <style media="screen">
              .icons,
              .texts {
                vertical-align: middle;
                display: inline-block;
              }
              </style>
              <!-- Style -->

              <!-- 3rdParty Source Link -->
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
              <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.bootstrap4.min.css">
              <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
              <!-- Source Link -->

              <!-- Content -->
              <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
              <!-- Archive Table -->
              <table id="example" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                      <tr>
                        <th>No. Document</th>
                        <th>Document</th>
                        <th>Type</th>
                        <th>Client</th>
                        <th>KTP</th>
                        <th>Contact</th>
                        <th>Created At</th>
                        <th>Last Update</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody >
                      <tr>
                        <!-- <td> -->
                          <!-- <img src="{{asset('/images/220px-Escaping_criticism-by_pere_borrel_del_caso.png')}}" width="100" height="50" /> -->
                          <!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                          <!-- <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                           viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                            <style type="text/css">
                                .st0no{fill:none;stroke:#575756;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                                .st1no{fill:none;stroke:#575756;stroke-width:10;stroke-linejoin:round;stroke-miterlimit:10;}
                                .st2no{fill:#FFFFFF;stroke:#36A9E1;stroke-width:20;stroke-linejoin:round;stroke-miterlimit:10;}
                                .st3no{fill:none;}
                                .st4no{fill:#575756;}
                                .st5no{font-family:'Calibri';}
                                .st6no{font-size:60px;}
                            </style>
                            <rect x="117" y="61.1" class="st0no" width="270" height="225"/>
                            <circle class="st1no" cx="247.6" cy="106.3" r="16.4"/>
                            <polyline class="st0no" points="114.9,225 197.3,138.4 272.3,220.9 "/>
                            <line class="st0no" x1="217.5" y1="286.1" x2="387" y2="84.9"/>
                            <path class="st2no" d="M607.8,349.3"/>
                            <rect x="108.2" y="311.4" class="st3no" width="289" height="127.5"/>
                            <text transform="matrix(1 0 0 1 137.1125 352.1154)"><tspan x="0" y="0" class="st4no st5no st6no">No Image</tspan><tspan x="7" y="72" class="st4 st5 st6"></tspan></text>
                          </svg>
                        </td> -->
                        <td>Antonio Bernard</td>
                        <td>Antonio Bernard</td>
                        <td>1298762146</td>

                        <td>2011/04/25</td>
                        <td>1298762146</td>

                        <td>2011/04/25</td>
                        <td>1298762146</td>

                        <td>2011/04/25</td>
                        <td>
                          <button type="button" class="btn btn-outline-primary">Detail</button>
                        </td>
                      </tr>
                      <tr>
                              <td align="center">
                                <img src="{{asset('/images/careerstrailerbox.jpg')}}" width="100" height="50" />
                              </td>
                              <td>Anastasya Kova</td>
                              <td>345435345</td>

                              <td>2011/04/25</td>
                              <td>345435345</td>

                              <td>2011/04/25</td>
                              <td>345435345</td>

                              <td>2011/04/25</td>
                              <td>
                                <button type="button" class="btn btn-outline-primary">Detail</button>
                              </td>
                          </tr>

                      </tbody>
                      <tfoot>
                          <tr>
                            <th>No. Document</th>
                            <th>Document</th>
                            <th>Type</th>
                            <th>Client</th>
                            <th>KTP</th>
                            <th>Contact</th>
                            <th>Created At</th>
                            <th>Last Update</th>
                            <th>Action</th>
                          </tr>
                      </tfoot>
                  </table>
                  <!-- Archive Table -->




<!-- =================================================================================================== Content =================================================================================================== -->
              <!-- JavaScript Bundle with Popper -->
              <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
              <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.bootstrap4.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
              <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.colVis.min.js"></script>
              <!-- JavaScript Bundle with Popper -->

              <!-- JS Function -->
              <script type="text/javascript">
              // Table Function
              $('#myModal').on('shown.bs.modal', function () {
                $('#myInput').trigger('focus')
              })

              $(document).ready(function() {
                var table = $('#example').DataTable( {
                    lengthChange: false,
                    buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                } );

                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
              } );
              // Table Function

                // function toggleFormOn() {
                //   document.getElementById("form").style.display = "block";
                //   document.getElementById("btnO").style.display = "none";
                //   document.getElementById("btnC").style.display = "block";
                // }
                // function toggleFormOff() {
                //   document.getElementById("form").style.display = "none";
                //   document.getElementById("btnO").style.display = "block";
                //   document.getElementById("btnC").style.display = "none";
                // }
              </script>
              <!-- JS Function -->

            </div>
        </div>
    </div>
</x-app-layout>
