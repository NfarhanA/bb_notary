<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TF Notary</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:light){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
                background-image: url("/images/Artboard_1.png");
                background-size: 100vw;
            }
            .icons,
            .texts {
              vertical-align: middle;
              display: inline-block;
            }
            div.bcard{
              background-color: rgba(255,255,255,0.9);
              /* background-color:grey; */
              border-radius: 25px;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen  sm:items-center py-4 sm:pt-0">
        <div class="bcard">
            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
                  <!-- <h1 style="color:Black;font-size:6vw;">NOTARY</h1> -->
                  <!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  	 viewBox="0 0 900 400" style="enable-background:new 0 0 900 400;" xml:space="preserve">
                  <style type="text/css">
                  	.st0l{fill:none;stroke:#1B1464;stroke-width:8;stroke-miterlimit:10;}
                  	.st1l{fill:none;}
                  	.st2l{fill:#1B1464;}
                  	.st3l{font-family: 'nunito';}
                  	.st4l{font-size:172px;}
                  	.st5l{letter-spacing:8;}
                  	.st6l{font-size:72px;}
                  	.st7l{letter-spacing:14;}
                  	.st8l{clip-path:url(#SVGID_2_);}
                  	.st9l{fill:#1B1464;stroke:#1B1464;stroke-miterlimit:10;}
                  	.st10l{fill:#2E3192;}
                  </style>
                  <g id="Layer_1">
                  	<circle class="st0l" cx="251.3" cy="182.5" r="153.7"/>
                  	<rect x="455.4" y="83.8" class="st1l" width="464.5" height="307.5"/>
                  	<text transform="matrix(1 0 0 1 455.4092 205.8996)" class="st2l st3l st4l st5l">Dwi</text>
                  	<rect x="463.5" y="221.5" class="st1l" width="454.6" height="70"/>
                  	<text transform="matrix(1 0 0 1 463.5342 272.6457)" class="st2l st3l st6l st7l">Notary</text>
                  </g>
                  <g id="Layer_4">
                  	<circle class="st1l" cx="251.3" cy="182.5" r="150"/>
                  </g>
                  <g id="Layer_2">
                  	<g>
                  		<defs>
                  			<circle id="SVGID_1_" cx="252.5" cy="182.5" r="150"/>
                  		</defs>
                  		<clipPath id="SVGID_2_">
                  			<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
                  		</clipPath>
                  		<g class="st8l">
                  			<path class="st2l" d="M217.5,173.2L203,200.4c8-0.2,25.1,0.4,43.6,10.1c19.4,10.1,29.8,24.7,34.1,31.4c4.8-9.1,9.7-18.1,14.5-27.2
                  				c-2.5-5.5-11.8-24.4-34.1-35.2C241.7,170,223.8,172.1,217.5,173.2z"/>
                  			<path class="st2l" d="M210.8,204.6l-20.2,95c1.3-0.2,4.5-0.4,7.9,1.2c4.1,2,6,5.4,6.5,6.5c22.1-23.7,44.1-47.3,66.2-71
                  				c-3.5-5.3-11.7-16.1-26.4-23.7C230.5,205.1,217.2,204.5,210.8,204.6z"/>
                  			<path class="st9l" d="M198.2,302.8l1.3-2.3c3.7,2,6.2,4.7,5.5,5.9c-0.7,1.3-4.3,0.7-8.1-1.3s-6.2-4.7-5.6-6
                  				c0.7-1.3,4.3-0.7,8.1,1.3L198.2,302.8z"/>
                  			<path class="st2l" d="M800.2-730.4l-505,945.1c-2.5-5.4-11.8-24.4-34-35.2c-19.4-9.5-37.2-7.3-43.6-6.3l505-945.1
                  				c9.5-17.8,33.8-23.6,53.9-12.8l4.4,2.3C801-771.6,809.7-748.2,800.2-730.4z"/>
                  			<path class="st10l" d="M237.8,172.7c0,0-16.6-0.1-20.2,0.5l505-945.1c3.3-6.3,14.7-14.1,14.7-14.1
                  				C633.3-568.5,432.1-209.4,237.8,172.7z"/>
                  			<path class="st2l" d="M203.4,199.9c-4.5,8.5,12.1,28.3,30.5,37.9c17.6,9.2,42.6,12,47.1,3.5c4.6-8.6-12.3-28.6-31-38.2
                  				C232.6,194.2,207.8,191.5,203.4,199.9z"/>
                  			<path class="st10l" d="M217.5,173.2c-4.3,8-8.7,16-13,24c1.4-0.7,3.5-1.7,6.2-2.3c1.5-0.3,3.7-0.6,9-0.3c2.8,0.2,6.6,0.6,11.1,1.5
                  				c1.6-3.9,3.3-8,5.1-12.1c1.7-3.8,3.4-7.5,5-11.1c-4.6-1-10.5-1.7-17.4-0.9C221.4,172.4,219.4,172.7,217.5,173.2z"/>
                  			<path class="st10l" d="M211.4,205.3c-6.6,31.1-13.2,62.3-19.9,93.4c0.5,0,1-0.1,1.5-0.1c0.9,0,1.7,0.1,2.4,0.3
                  				c5.2-14.9,10.6-30,16.3-45.3c5.8-15.7,11.6-31.1,17.5-46.1c-3.2-0.8-6.9-1.5-10.9-1.9C215.8,205.4,213.5,205.3,211.4,205.3z"/>
                  		</g>
                  	</g>
                  </g>
                  <g id="Layer_3">
                  	<path class="st9l" d="M192.9,301.8"/>
                  	<g>
                  		<path class="st2" d="M122.5,138.9c-0.5,16.7,1,32.9,4.2,48.9c3.2,15.9,8.1,31.4,14.5,46.1c6.5,14.7,14.6,28.7,24.7,41.3
                  			c10,12.6,22,23.7,36.2,32.4c-2-0.6-4-1.3-5.9-2.1c-1.9-0.8-3.9-1.6-5.8-2.5c-1.9-1-3.8-1.9-5.6-2.9l-2.7-1.6
                  			c-0.9-0.5-1.8-1.1-2.7-1.7c-7.1-4.7-13.6-10.2-19.5-16.4c-11.8-12.3-21-26.9-27.9-42.5c-6.8-15.6-11.3-32.1-13.1-48.9
                  			C117.2,172.2,118,155,122.5,138.9z"/>
                  	</g>
                  </g>
                  </svg>
                </div>
                <div class="mt-8  ">
                    @if (Route::has('login'))
                      @auth
                      <div class="grid">
                        <div class="p-6">
                            <div class="flex items-center">
                                <div class="ml-4 text-lg leading-7 font-semibold">
                                  <a href="{{ url('/dashboard') }}" class=" text-gray-900 dark:text-white">
                                    <button type="button" class="btn btn-outline-primary  btn-lg">
                                      <span class="icons">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        width="30px" height="30px"	 viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
                                        <style type="text/css">
                                        	.st0{fill:#0071BC;}
                                        	.st1{fill:lightblue;}
                                        </style>
                                        <g>
                                        		<rect x="108.58" y="108.58" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -103.5534 250)" class="st0" width="282.84" height="282.84"/>
                                        </g>
                                        <g>
                                        	<rect x="13" y="231" class="st1" width="474" height="38"/>
                                        </g>
                                        <g>
                                        	<rect x="231" y="13" class="st1" width="38" height="474"/>
                                        </g>
                                        </svg>
                                      </span>
                                      &nbsp;
                                    <span class="texts"> Dashboard </span></button>
                                  </a>
                                </div>
                            </div>

                            <div class="ml-12">
                                  <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                  </div>
                            </div>
                        </div>
                    @else
                    <div class="grid grid-cols-1 md:grid-cols-2">
                        <div class="p-6 ">
                            <div class="flex items-center">
                                <div class="ml-4 text-lg leading-7 font-semibold">
                                  <a href="{{ route('login') }}" class="text-gray-900 dark:text-white">
                                    <button type="button" class="btn btn-outline-primary  btn-lg">
                                      <span class="icons">
                                        <!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="30px" height="30px"	 viewBox="0 0 1417.32 1417.32" style="enable-background:new 0 0 1417.32 1417.32;" xml:space="preserve">
                                        <style type="text/css">
                                        	.st0{fill:#36A9E1;}
                                        </style>
                                        <rect x="1204.72" y="28.35" class="st0" width="141.73" height="1360.63"/>
                                        <rect x="61.59" y="644.88" class="st0" width="680.31" height="127.56"/>
                                        <polygon class="st0" points="945.49,708.66 584.7,347.87 785.14,347.87 1145.93,708.66 "/>
                                        <polygon class="st0" points="945.49,708.66 584.7,1069.45 785.14,1069.45 1145.93,708.66 "/>
                                        </svg>
                                    </span>
                                    &nbsp;
                                    <span class="texts"> Login </span></button>
                                  </a>
                                </div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                  </div>
                            </div>
                        </div>
                        @if (Route::has('register'))
                        <div class="p-6 ">
                            <div class="flex items-center">
                              <div class="ml-4 text-lg leading-7 font-semibold text-gray-900 dark:text-white">
                                <a href="{{ route('register') }}" class=" text-gray-900 dark:text-white">
                                    <button type="button" class="btn btn-outline-primary  btn-lg">
                                      <span class="texts">
                                      <!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                      	 width="30px" height="30px"viewBox="0 0 1417.32 1417.32" style="enable-background:new 0 0 1417.32 1417.32;" xml:space="preserve">
                                      <style type="text/css">
                                      	.st0{fill:#36A9E1;}
                                      	.st1{fill:#FFFFFF;stroke:#36A9E1;stroke-width:5;stroke-miterlimit:10;}
                                      </style>
                                      <circle class="st0" cx="616.66" cy="300.98" r="170.08"/>
                                      <path class="st0" d="M45.26,572.07"/>
                                      <path class="st0" d="M861.82,690.27"/>
                                      <path class="st0" d="M793.83,480.56"/>
                                      <path class="st0" d="M624.29,544.38"/>
                                      <path class="st0" d="M1019.54,572.07"/>
                                      <path class="st0" d="M623.95,544.38"/>
                                      <path class="st0" d="M624.29,544.38c-323.91-0.49-336.44,235.16-336.44,235.16v506.89h336.44V544.38z"/>
                                      <path class="st0" d="M623.95,544.38c323.91-0.49,336.44,235.16,336.44,235.16v506.89H623.95V544.38z"/>
                                      <circle class="st1" cx="986.81" cy="1077.17" r="212.6"/>
                                      <rect x="960.24" y="928.35" class="st0" width="53.15" height="297.64"/>
                                      <rect x="960.24" y="928.35" transform="matrix(4.493656e-11 -1 1 4.493656e-11 -90.3543 2063.9763)" class="st0" width="53.15" height="297.64"/>
                                      </svg>
                                    </span>
                                    &nbsp;
                                    <span class="texts"> Register </span></button>
                                  </a>
                                </div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    </div>
                            </div>
                        </div>
                        @endif
                        @endauth
                        @endif
                    </div>
                </div>

                <div class="flex justify-center mt-4 sm:items-center sm:justify-between">


                    <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
                        Using Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
                    </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </body>
</html>
